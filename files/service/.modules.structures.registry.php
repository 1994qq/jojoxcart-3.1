# <?php if (!defined('LC_DS')) { die(); } ?>

XC\ThemeTweaker:
    tables: [theme_tweaker_template]
    columns: { view_lists: { list_override: 'list_override VARCHAR(255) NOT NULL', weight_override: 'weight_override INT NOT NULL', override_mode: 'override_mode INT NOT NULL' } }
    dependencies: {  }
XC\Onboarding:
    tables: {  }
    columns: { categories: { demo: 'demo TINYINT(1) NOT NULL' }, orders: { demo: 'demo TINYINT(1) NOT NULL' }, products: { demo: 'demo TINYINT(1) NOT NULL' } }
    dependencies: {  }
XC\UPS:
    tables: {  }
    columns: {  }
    dependencies: {  }
XC\EPDQ:
    tables: {  }
    columns: {  }
    dependencies: {  }
XC\FastLaneCheckout:
    tables: {  }
    columns: {  }
    dependencies: {  }
XC\GbTranslation:
    tables: {  }
    columns: {  }
    dependencies: {  }
XC\CrispWhiteSkin:
    tables: {  }
    columns: {  }
    dependencies: {  }
XC\NewsletterSubscriptions:
    tables: [newsletter_subscriptions_subscribers]
    columns: {  }
    dependencies: {  }
XC\IdealPayments:
    tables: {  }
    columns: {  }
    dependencies: {  }
XC\SagePay:
    tables: {  }
    columns: {  }
    dependencies: {  }
XC\MailChimp:
    tables: [mailchimp_list_segments, segment_membership, segment_products, mailchimp_segment_subscriptions, mailchimp_list_group, mailchimp_store, mailchimp_lists, mailchimp_subscriptions, mailchimp_list_group_name, mailchimp_profile_interests]
    columns: { orders: { mailchimpStoreId: 'mailchimpStoreId VARCHAR(255) NOT NULL' }, products: { useAsSegmentCondition: 'useAsSegmentCondition TINYINT(1) NOT NULL' } }
    dependencies: {  }
XC\Stripe:
    tables: {  }
    columns: {  }
    dependencies: {  }
XC\FroalaEditor:
    tables: {  }
    columns: {  }
    dependencies: {  }
XC\Concierge:
    tables: {  }
    columns: { profiles: { conciergeUserId: 'conciergeUserId VARCHAR(128) DEFAULT NULL' } }
    dependencies: {  }
CDev\TwoCheckout:
    tables: {  }
    columns: {  }
    dependencies: {  }
CDev\Quantum:
    tables: {  }
    columns: {  }
    dependencies: {  }
CDev\VAT:
    tables: [vat_taxes, vat_tax_translations, vat_tax_rates]
    columns: { order_items: { vatRate: 'vatRate VARCHAR(32) DEFAULT NULL' } }
    dependencies: {  }
CDev\Moneybookers:
    tables: {  }
    columns: {  }
    dependencies: {  }
CDev\GoogleAnalytics:
    tables: {  }
    columns: { profiles: { gaClientId: 'gaClientId VARCHAR(255) NOT NULL' }, order_items: { categoryAdded: 'categoryAdded VARCHAR(255) DEFAULT NULL' } }
    dependencies: {  }
CDev\FedEx:
    tables: {  }
    columns: {  }
    dependencies: {  }
CDev\AuthorizeNet:
    tables: {  }
    columns: {  }
    dependencies: {  }
CDev\GoSocial:
    tables: {  }
    columns: { pages: { useCustomOG: 'useCustomOG TINYINT(1) NOT NULL', ogMeta: 'ogMeta LONGTEXT NOT NULL', showSocialButtons: 'showSocialButtons TINYINT(1) NOT NULL' }, categories: { ogMeta: 'ogMeta LONGTEXT NOT NULL', useCustomOG: 'useCustomOG TINYINT(1) NOT NULL' }, products: { ogMeta: 'ogMeta LONGTEXT DEFAULT NULL', useCustomOG: 'useCustomOG TINYINT(1) NOT NULL' } }
    dependencies: { CDev\SimpleCMS: { pages: { useCustomOG: 'useCustomOG TINYINT(1) NOT NULL', ogMeta: 'ogMeta LONGTEXT NOT NULL', showSocialButtons: 'showSocialButtons TINYINT(1) NOT NULL' } } }
CDev\XPaymentsConnector:
    tables: [xpc_payment_transaction_data, xpc_payment_fraud_check_data, xpc_payment_data_cells]
    columns: { orders: { fraud_status_xpc: 'fraud_status_xpc VARCHAR(255) NOT NULL', fraud_type_xpc: 'fraud_type_xpc VARCHAR(255) NOT NULL', fraud_check_transaction_id: 'fraud_check_transaction_id INT NOT NULL', is_zero_auth: 'is_zero_auth TINYINT(1) NOT NULL' }, profiles: { default_card_id: 'default_card_id INT UNSIGNED NOT NULL', pending_zero_auth: 'pending_zero_auth VARCHAR(255) NOT NULL', pending_zero_auth_txn_id: 'pending_zero_auth_txn_id VARCHAR(255) NOT NULL', pending_zero_auth_status: 'pending_zero_auth_status CHAR(1) NOT NULL', pending_zero_auth_interface: 'pending_zero_auth_interface VARCHAR(255) NOT NULL' }, order_items: { xpcFakeItem: 'xpcFakeItem TINYINT(1) NOT NULL' } }
    dependencies: {  }
CDev\SimpleCMS:
    tables: [pages, page_images, page_translations, menus, menu_translations, menu_quick_flags]
    columns: { clean_urls: { page_id: 'page_id INT UNSIGNED DEFAULT NULL' } }
    dependencies: {  }
CDev\ContactUs:
    tables: {  }
    columns: {  }
    dependencies: {  }
CDev\FeaturedProducts:
    tables: [featured_products]
    columns: {  }
    dependencies: {  }
CDev\XMLSitemap:
    tables: {  }
    columns: {  }
    dependencies: {  }
CDev\Bestsellers:
    tables: {  }
    columns: {  }
    dependencies: {  }
QSL\CloudSearch:
    tables: {  }
    columns: {  }
    dependencies: {  }
QSL\FlyoutCategoriesMenu:
    tables: {  }
    columns: {  }
    dependencies: {  }
qq\CustomiseModule:
    tables: {  }
    columns: {  }
    dependencies: {  }
qq\ProductDesignId:
    tables: {  }
    columns: { order_items: { designId: 'designId VARCHAR(255) NOT NULL' } }
    dependencies: {  }
qq\EditDesignButton:
    tables: {  }
    columns: {  }
    dependencies: {  }
CDev\Paypal:
    tables: {  }
    columns: {  }
    dependencies: {  }
