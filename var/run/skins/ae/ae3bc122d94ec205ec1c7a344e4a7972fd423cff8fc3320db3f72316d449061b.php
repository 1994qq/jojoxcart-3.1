<?php

/* product/details/parts/image-regular.twig */
class __TwigTemplate_0c64dd6358e397d8a0b7277caac194dee49e41aaeaaee7681c05f2ce2bfe528e extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"product-photo lazy-load\">
  <div class=\"image-flex-item\">
    ";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\Module\\XC\\CrispWhiteSkin\\View\\CommonImage", "isBlurApplicable" => true, "image" => $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "product", array()), "getImage", array(), "method"), "className" => "photo product-thumbnail", "verticalAlign" => "top", "id" => ("product_image_" . $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "product", array()), "product_id", array())), "maxWidth" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getWidgetMaxWidth", array(), "method"), "maxHeight" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getWidgetMaxHeight", array(), "method"), "alt" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getAlt", array(0 => $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "product", array()), "getImage", array(), "method")), "method")))), "html", null, true);
        echo "
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "product/details/parts/image-regular.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Product details image default box*/
/*  #}*/
/* <div class="product-photo lazy-load">*/
/*   <div class="image-flex-item">*/
/*     {{ widget('\\XLite\\Module\\XC\\CrispWhiteSkin\\View\\CommonImage', isBlurApplicable=true, image=this.product.getImage(), className='photo product-thumbnail', verticalAlign='top', id='product_image_' ~ this.product.product_id, maxWidth=this.getWidgetMaxWidth(), maxHeight=this.getWidgetMaxHeight(), alt=this.getAlt(this.product.getImage())) }}*/
/*   </div>*/
/* </div>*/
/* */
