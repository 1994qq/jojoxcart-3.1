<?php

/* /var/www/html/xcart/skins/admin/zones/details/parts/actions.twig */
class __TwigTemplate_6d5714c07fe292b492c14daa7355dd04c918f75c23d28dde595c20c2c068a6db extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\StickyPanel\\ZoneDetails"))), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/zones/details/parts/actions.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Zone details actions block template*/
/*  #*/
/*  # @ListChild (list="zones.zone", weight="300")*/
/*  #}*/
/* */
/* {{ widget('\\XLite\\View\\StickyPanel\\ZoneDetails') }}*/
/* */
