<?php

/* /var/www/html/xcart/skins/admin/header/parts/meta_mobile-capable.twig */
class __TwigTemplate_cf7dd577ca41e172ad7a22ece167692ad509227b18c430cfae52a1ca9bea2a61 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<meta name=\"apple-mobile-web-app-capable\"   content=\"yes\" />
<meta name=\"mobile-web-app-capable\"         content=\"yes\" />";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/header/parts/meta_mobile-capable.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Head list children*/
/*  #*/
/*  # @ListChild (list="head", weight="700")*/
/*  #}*/
/* */
/* <meta name="apple-mobile-web-app-capable"   content="yes" />*/
/* <meta name="mobile-web-app-capable"         content="yes" />*/
