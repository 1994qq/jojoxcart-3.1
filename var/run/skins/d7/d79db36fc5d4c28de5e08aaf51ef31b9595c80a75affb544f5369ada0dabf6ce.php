<?php

/* /var/www/html/xcart/skins/customer/checkout/success/parts/invoice.twig */
class __TwigTemplate_e67156f47e660f9cbbce2bc7f38e4ebbb81f3a8c2247e55baf1a3053c42b8b50 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Invoice", "order" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getOrder", array(), "method")))), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/checkout/success/parts/invoice.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Invoice*/
/*  #*/
/*  # @ListChild (list="checkout.success", weight="200")*/
/*  #}*/
/* */
/* {{ widget('\\XLite\\View\\Invoice', order=this.getOrder()) }}*/
/* */
