<?php

/* /var/www/html/xcart/skins/customer/modules/XC/FastLaneCheckout/sections/shipping/note.twig */
class __TwigTemplate_c5f1326181c766581c2c4bf5ffbfc382680bd666924a0dc03a6c45b506b3d5a7 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<div class='checkout_fastlane_order_note'>
\t";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\Module\\XC\\FastLaneCheckout\\View\\Blocks\\OrderNotes"))), "html", null, true);
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/modules/XC/FastLaneCheckout/sections/shipping/note.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 8,  19 => 6,);
    }
}
/* {##*/
/*  # Fastlane checkout address section*/
/*  #*/
/*  # @ListChild (list="checkout_fastlane.sections.shipping.left", weight="30")*/
/*  #}*/
/* */
/* <div class='checkout_fastlane_order_note'>*/
/* 	{{ widget('XLite\\Module\\XC\\FastLaneCheckout\\View\\Blocks\\OrderNotes') }}*/
/* </div>*/
