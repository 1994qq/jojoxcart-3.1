<?php

/* shopping_cart/parts/box.estimator.twig */
class __TwigTemplate_755008da4826f309057a38327a5a7e7c70ec217955db3aeeec218954fb388e99 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"estimator\" data-deferred=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "shouldDeferLoad", array(), "method"), "html", null, true);
        echo "\" data-shipping-cost=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getShippingCost", array(), "method"), "html", null, true);
        echo "\">

  ";
        // line 6
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isShippingEstimate", array(), "method")) {
            // line 7
            echo "
    <ul>
      <li>
        <span class=\"section\">";
            // line 10
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Shipping")), "html", null, true);
            echo ":</span>
        ";
            // line 11
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\ShippingEstimator\\SelectedMethod"))), "html", null, true);
            echo "
      </li>
      <li>
        <span class=\"section\">";
            // line 14
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Estimated for")), "html", null, true);
            echo ":</span>
        ";
            // line 15
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getEstimateAddress", array(), "method"), "html", null, true);
            echo "
      </li>
    </ul>

    <div class=\"link\">
      <a href=\"";
            // line 20
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('url')->getCallable(), array($this->env, $context, "shipping_estimate")), "html", null, true);
            echo "\" class=\"estimate\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Change method")), "html", null, true);
            echo "</a>
    </div>

  ";
        } else {
            // line 24
            echo "
    ";
            // line 25
            $this->startForm("XLite\\View\\Form\\Cart\\ShippingEstimator\\Open");            // line 26
            echo "      <div class=\"buttons\">
        ";
            // line 27
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Button\\Submit", "label" => "Estimate shipping cost", "style" => "estimate"))), "html", null, true);
            echo "
      </div>
    ";
            $this->endForm();            // line 30
            echo "
  ";
        }
        // line 32
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "shopping_cart/parts/box.estimator.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 32,  77 => 30,  72 => 27,  69 => 26,  68 => 25,  65 => 24,  56 => 20,  48 => 15,  44 => 14,  38 => 11,  34 => 10,  29 => 7,  27 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Shipping estimator box*/
/*  #}*/
/* <div class="estimator" data-deferred="{{ this.shouldDeferLoad() }}" data-shipping-cost="{{ this.getShippingCost() }}">*/
/* */
/*   {% if this.isShippingEstimate() %}*/
/* */
/*     <ul>*/
/*       <li>*/
/*         <span class="section">{{ t('Shipping') }}:</span>*/
/*         {{ widget('XLite\\View\\ShippingEstimator\\SelectedMethod') }}*/
/*       </li>*/
/*       <li>*/
/*         <span class="section">{{ t('Estimated for') }}:</span>*/
/*         {{ this.getEstimateAddress() }}*/
/*       </li>*/
/*     </ul>*/
/* */
/*     <div class="link">*/
/*       <a href="{{ url('shipping_estimate') }}" class="estimate">{{ t('Change method') }}</a>*/
/*     </div>*/
/* */
/*   {% else %}*/
/* */
/*     {% form 'XLite\\View\\Form\\Cart\\ShippingEstimator\\Open' %}*/
/*       <div class="buttons">*/
/*         {{ widget('XLite\\View\\Button\\Submit', label='Estimate shipping cost', style='estimate') }}*/
/*       </div>*/
/*     {% endform %}*/
/* */
/*   {% endif %}*/
/* */
/* </div>*/
/* */
