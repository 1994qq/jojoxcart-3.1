<?php

/* /var/www/html/xcart/skins/admin/order/packing_slip/parts/body/items/item.name.sku.twig */
class __TwigTemplate_f15bdfe2686e27fcafcb90c38deb46c1e75a3649d04271c79ef0b2f1710103b9 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<td class=\"sku\">
  <span class=\"sku-value\">";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "item", array()), "getSku", array(), "method"), "html", null, true);
        echo "</span>
</td>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/order/packing_slip/parts/body/items/item.name.sku.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # SKU item cell*/
/*  #*/
/*  # @ListChild (list="packing_slip.item", weight="10")*/
/*  #}*/
/* <td class="sku">*/
/*   <span class="sku-value">{{ this.item.getSku() }}</span>*/
/* </td>*/
/* */
