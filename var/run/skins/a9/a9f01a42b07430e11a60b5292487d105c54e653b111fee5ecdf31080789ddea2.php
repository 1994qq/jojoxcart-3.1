<?php

/* /var/www/html/xcart/skins/theme_tweaker/customer/shopping_cart/parts/item.info.twig */
class __TwigTemplate_526d7cf78137afd142e9d116bddfbb78e4e8733b0b85598c6bed9e91955a98ab extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<td class=\"item-info\">

 ";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "cart.item.info", "item" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "item", array())))), "html", null, true);
        echo "

";
        // line 10
        if ($this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "item", array()), "getDesignId", array(), "method")) {
            // line 11
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\Module\\qq\\EditDesignButton\\View\\Page\\Button\\DemoEditDesignButton", "label" => "Edit Your Design", "item" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "item", array())))), "html", null, true);
            echo "
";
        }
        // line 13
        echo "
</td>

";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/theme_tweaker/customer/shopping_cart/parts/item.info.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 13,  30 => 11,  28 => 10,  23 => 8,  19 => 6,);
    }
}
/* {##*/
/*  # Shopping cart item : info*/
/*  #*/
/*  # @ListChild (list="cart.item", weight="30")*/
/*  #}*/
/* <td class="item-info">*/
/* */
/*  {{ widget_list('cart.item.info', item=this.item) }}*/
/* */
/* {% if this.item.getDesignId() %}*/
/* {{ widget('\\XLite\\Module\\qq\\EditDesignButton\\View\\Page\\Button\\DemoEditDesignButton', label='Edit Your Design', item=this.item) }}*/
/* {% endif %}*/
/* */
/* </td>*/
/* */
/* */
