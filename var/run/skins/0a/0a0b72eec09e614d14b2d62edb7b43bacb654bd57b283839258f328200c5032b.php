<?php

/* /var/www/html/xcart/skins/admin/modules/CDev/FeaturedProducts/items_list/product/featured/parts/header/checkbox.twig */
class __TwigTemplate_106caabcf40c664757731a2c3b0360e61d1ac006a2c480e888972da09c93dd68 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<th class=\"checkboxes\"><input type=\"checkbox\" class=\"column-selector\" /></th>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/modules/CDev/FeaturedProducts/items_list/product/featured/parts/header/checkbox.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Column with checkboxes*/
/*  #*/
/*  # @ListChild (list="itemsList.product.admin.featured.header", weight="10")*/
/*  #}*/
/* */
/* <th class="checkboxes"><input type="checkbox" class="column-selector" /></th>*/
/* */
