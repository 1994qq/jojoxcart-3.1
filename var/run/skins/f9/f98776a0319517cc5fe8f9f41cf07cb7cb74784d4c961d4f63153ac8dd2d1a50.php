<?php

/* /var/www/html/xcart/skins/admin/payment/appearance/note.twig */
class __TwigTemplate_ef6ebe672ed5376c761f0aa70b3eb8365d7c8fb45528998345cc552b7daf668a extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<div class=\"alert alert-info\" role=\"alert\">
  ";
        // line 8
        echo call_user_func_array($this->env->getFunction('t')->getCallable(), array("Payment methods appearance description"));
        echo "
</div>

";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/payment/appearance/note.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 8,  19 => 6,);
    }
}
/* {##*/
/*  # Payment methods. Appearance tab.*/
/*  #*/
/*  # @ListChild (list="itemsList.methods.header", weight="10")*/
/*  #}*/
/* */
/* <div class="alert alert-info" role="alert">*/
/*   {{ t('Payment methods appearance description')|raw }}*/
/* </div>*/
/* */
/* */
