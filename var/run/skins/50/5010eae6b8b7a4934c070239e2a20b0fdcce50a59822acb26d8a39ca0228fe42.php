<?php

/* /var/www/html/xcart/skins/admin/items_list/module/install/parts/header.twig */
class __TwigTemplate_23ac35cfb9f61ea0ec77825749539d3616c82c96c6267a0036aaa4faa9c0a2d9 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isLandingPage", array(), "method")) {
            // line 7
            echo "  <h2>";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Hot addons")), "html", null, true);
            echo "</h2>
";
        }
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/items_list/module/install/parts/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  21 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Landing page header*/
/*  #*/
/*  # @ListChild (list="marketplace.top-controls", weight="400")*/
/*  #}*/
/* {% if this.isLandingPage() %}*/
/*   <h2>{{ t('Hot addons') }}</h2>*/
/* {% endif %}*/
/* */
