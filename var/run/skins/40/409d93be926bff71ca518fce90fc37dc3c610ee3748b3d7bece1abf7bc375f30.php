<?php

/* modules/CDev/VAT/price.twig */
class __TwigTemplate_e0b3ea3537be0476e369add27efe99cfb9c16425aafac41f70fb1a5a9bc7dd95 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 5
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isVATApplicable", array(), "method")) {
            // line 6
            echo "  ";
            if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isDisplayedPriceIncludingVAT", array(), "method")) {
                // line 7
                echo "    <li class=\"vat-price\"><span class=\"vat-note-product-price\">";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("incl.VAT", array("name" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getVATName", array(), "method")))), "html", null, true);
                echo "</span>
    </li>
  ";
            } else {
                // line 10
                echo "    <li class=\"vat-price\"><span class=\"vat-note-product-price\">";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("excl.VAT", array("name" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getVATName", array(), "method")))), "html", null, true);
                echo "</span>
    </li>
  ";
            }
            // line 13
            echo "  <br>
";
        }
    }

    public function getTemplateName()
    {
        return "modules/CDev/VAT/price.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 13,  34 => 10,  27 => 7,  24 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Product price value*/
/*  #}*/
/* */
/* {% if this.isVATApplicable() %}*/
/*   {% if this.isDisplayedPriceIncludingVAT() %}*/
/*     <li class="vat-price"><span class="vat-note-product-price">{{ t('incl.VAT', {'name': this.getVATName()}) }}</span>*/
/*     </li>*/
/*   {% else %}*/
/*     <li class="vat-price"><span class="vat-note-product-price">{{ t('excl.VAT', {'name': this.getVATName()}) }}</span>*/
/*     </li>*/
/*   {% endif %}*/
/*   <br>*/
/* {% endif %}*/
/* */
