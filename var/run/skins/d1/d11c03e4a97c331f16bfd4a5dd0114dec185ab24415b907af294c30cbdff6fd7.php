<?php

/* modules/XC/ThemeTweaker/themetweaker/webmaster_mode/webmaster_mode.twig */
class __TwigTemplate_9838b4ad10fb2a2a3181a478df5f3caae1f5b221cc295328939fd6eb4086cfa9 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<xlite-webmaster-mode inline-template interface=\"";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getInterface", array(), "method"), "html", null, true);
        echo "\" tree-key=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getJstreeCacheKey", array(), "method"), "html", null, true);
        echo "\">
  <div class=\"webmaster-mode-section themetweaker-section\">
    <div class=\"webmaster-mode-tree\" :class=\"treeClasses\">
      <div id=\"themeTweaker_wrapper\" style=\"display: none;\" data-editor-wrapper>
        <div class=\"themeTweaker-control-panel\" data-editor-control-panel>
        </div>
      </div>
    </div>
    <div class=\"webmaster-mode-code\" data-editor-code>
      ";
        // line 14
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\Module\\XC\\ThemeTweaker\\View\\ThemeTweaker\\TemplateCode"))), "html", null, true);
        echo "
    </div>
  </div>
</xlite-webmaster-mode>";
    }

    public function getTemplateName()
    {
        return "modules/XC/ThemeTweaker/themetweaker/webmaster_mode/webmaster_mode.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 14,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Layout editor panel*/
/*  #}*/
/* */
/* <xlite-webmaster-mode inline-template interface="{{ this.getInterface() }}" tree-key="{{ this.getJstreeCacheKey() }}">*/
/*   <div class="webmaster-mode-section themetweaker-section">*/
/*     <div class="webmaster-mode-tree" :class="treeClasses">*/
/*       <div id="themeTweaker_wrapper" style="display: none;" data-editor-wrapper>*/
/*         <div class="themeTweaker-control-panel" data-editor-control-panel>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*     <div class="webmaster-mode-code" data-editor-code>*/
/*       {{ widget('XLite\\Module\\XC\\ThemeTweaker\\View\\ThemeTweaker\\TemplateCode') }}*/
/*     </div>*/
/*   </div>*/
/* </xlite-webmaster-mode>*/
