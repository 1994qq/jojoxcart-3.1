<?php

/* /var/www/html/xcart/skins/customer/modules/CDev/Paypal/product/details/parts/express_checkout.twig */
class __TwigTemplate_92b5a91075687fdc656926e0475880120f81d250ec6a0116f96d1da95c0a1662 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\Module\\CDev\\Paypal\\View\\Product\\ExpressCheckoutButton", "product" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "product", array())))), "html", null, true);
        echo "

";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/modules/CDev/Paypal/product/details/parts/express_checkout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 9,);
    }
}
/* {##*/
/*  # Express checkout*/
/*  #*/
/*  # @ListChild (list="product.details.page.info.buttons-added.cart-buttons", weight="40")*/
/*  # @ListChild (list="product.details.quicklook.info.buttons-added.cart-buttons", weight="40")*/
/*  # @ListChild (list="product.details.page.info.buttons.cart-buttons", weight="30")*/
/*  # @ListChild (list="product.details.quicklook.info.buttons.cart-buttons", weight="30")*/
/*  #}*/
/* {{ widget('\\XLite\\Module\\CDev\\Paypal\\View\\Product\\ExpressCheckoutButton', product=this.product) }}*/
/* */
/* */
