<?php

/* /var/www/html/xcart/skins/admin/header/parts/noindex.twig */
class __TwigTemplate_4cdf54f04d9fa5d6653642c76064b8eaeec38422c6137869a181a8d2c968f477 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<meta name=\"robots\" content=\"noindex, nofollow\" />
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/header/parts/noindex.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Header part*/
/*  #*/
/*  # @ListChild (list="head", weight="40")*/
/*  #}*/
/* */
/* <meta name="robots" content="noindex, nofollow" />*/
/* */
