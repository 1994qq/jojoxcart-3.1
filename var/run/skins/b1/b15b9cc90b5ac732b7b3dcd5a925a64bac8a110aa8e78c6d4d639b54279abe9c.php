<?php

/* /var/www/html/xcart/skins/customer/product/details/parts/page.tabs.twig */
class __TwigTemplate_8a1da8cc1295a70c1ba8001bb4243cd22f1910d493e3d68bbe42b396e1c63c65 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Product\\Details\\Customer\\Page\\Tabs"))), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/product/details/parts/page.tabs.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Product details information block*/
/*  #*/
/*  # @ListChild (list="product.details.page", weight="40")*/
/*  #}*/
/* */
/* {{ widget('XLite\\View\\Product\\Details\\Customer\\Page\\Tabs') }}*/
/* */
