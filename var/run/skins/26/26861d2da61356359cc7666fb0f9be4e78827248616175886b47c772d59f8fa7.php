<?php

/* /var/www/html/xcart/skins/customer/checkout/steps/review/parts/place_order.button.twig */
class __TwigTemplate_d9f67168cf6a911b2e09fd9e7140a00a126d95c15dae51dd32145cce9980ee3b extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<div class=\"button-row\">
  ";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Button\\PlaceOrder"))), "html", null, true);
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/checkout/steps/review/parts/place_order.button.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 8,  19 => 6,);
    }
}
/* {##*/
/*  # Checkout : order review step : selected state : place order : button*/
/*  #*/
/*  # @ListChild (list="checkout.review.selected.placeOrder", weight="400")*/
/*  #}*/
/* */
/* <div class="button-row">*/
/*   {{ widget('\\XLite\\View\\Button\\PlaceOrder') }}*/
/* </div>*/
/* */
