<?php

/* /var/www/html/xcart/skins/customer/header/parts/meta_generator.twig */
class __TwigTemplate_030a83e7dea022260a30d6a9e429862bf3e184369969c8f37886075fd9bf96bd extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<meta name=\"Generator\" content=\"X-Cart 5\" />
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/header/parts/meta_generator.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Head list children*/
/*  #*/
/*  # @ListChild (list="head", weight="500")*/
/*  #}*/
/* */
/* <meta name="Generator" content="X-Cart 5" />*/
/* */
