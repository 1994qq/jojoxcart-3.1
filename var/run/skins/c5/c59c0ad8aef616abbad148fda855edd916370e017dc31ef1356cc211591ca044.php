<?php

/* /var/www/html/xcart/skins/admin/modules/CDev/Paypal/order/payment.id.twig */
class __TwigTemplate_b1be64f428eaced039b16a4368a6e80ba89a4e19c1694bcb9104978371c2920e extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<!--div class=\"pp-transaction-ids\" IF=\"order.getTransactionIds()\">
  {foreach:order.getTransactionIds(),tid}
  <div>
    {tid.name}:
    {if:tid.url}
      <a href=\"{tid.url}\">{tid.value}</a>
    {else:}
      {tid.value}
    {end:}
  </div>
  {end:}
</div-->
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/modules/CDev/Paypal/order/payment.id.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Payment transaction PNREF and PPREF (for PayPal transactions only)*/
/*  #*/
/*  # @ListChild (list="order.payment", weight="110")*/
/*  #}*/
/* */
/* <!--div class="pp-transaction-ids" IF="order.getTransactionIds()">*/
/*   {foreach:order.getTransactionIds(),tid}*/
/*   <div>*/
/*     {tid.name}:*/
/*     {if:tid.url}*/
/*       <a href="{tid.url}">{tid.value}</a>*/
/*     {else:}*/
/*       {tid.value}*/
/*     {end:}*/
/*   </div>*/
/*   {end:}*/
/* </div-->*/
/* */
