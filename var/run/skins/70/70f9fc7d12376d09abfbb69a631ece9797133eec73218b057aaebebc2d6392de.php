<?php

/* back_to_top/body.twig */
class __TwigTemplate_fb04e65bae6715ce777bc56f5f8c9284bea9ccef19ef3ce4255ee569dff206e5 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<a href=\"#0\" class=\"back-to-top\" title=\"Back to top\">
  <i class=\"custom-icon\" aria-hidden=\"true\"></i>
</a>
";
    }

    public function getTemplateName()
    {
        return "back_to_top/body.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 4,);
    }
}
/* {##*/
/*  # BackToTop button template*/
/*  #}*/
/* */
/* <a href="#0" class="back-to-top" title="Back to top">*/
/*   <i class="custom-icon" aria-hidden="true"></i>*/
/* </a>*/
/* */
