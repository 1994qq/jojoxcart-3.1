<?php

/* /var/www/html/xcart/skins/crisp_white/customer/account/parts/link.login.twig */
class __TwigTemplate_e161a67e5939f6ec59f94ed6d176ad1cb14946044f202ffdb1185cf98874909e extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<div class=\"additional-buttons text-center\">
";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Button\\PopupLoginLink"))), "html", null, true);
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/crisp_white/customer/account/parts/link.login.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 8,  19 => 6,);
    }
}
/* {##*/
/*  # Link : create account*/
/*  #*/
/*  # @ListChild (list="customer.account.details.after", weight="100")*/
/*  #}*/
/* */
/* <div class="additional-buttons text-center">*/
/* {{ widget('XLite\\View\\Button\\PopupLoginLink') }}*/
/* </div>*/
/* */
