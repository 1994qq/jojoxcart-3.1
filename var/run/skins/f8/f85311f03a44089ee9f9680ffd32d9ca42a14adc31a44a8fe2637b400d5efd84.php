<?php

/* /var/www/html/xcart/skins/customer/modules/CDev/VAT/price.twig */
class __TwigTemplate_bf88460956d5d52a88404ad8bd27cb8694eb8504efa503318a9b955a02397a4f extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 7
        echo "
";
        // line 8
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isVATApplicable", array(), "method")) {
            // line 9
            echo "  ";
            if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isDisplayedPriceIncludingVAT", array(), "method")) {
                // line 10
                echo "    <li class=\"vat-price\"><span class=\"vat-note-product-price\">";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("incl.VAT", array("name" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getVATName", array(), "method")))), "html", null, true);
                echo "</span>
    </li>
  ";
            } else {
                // line 13
                echo "    <li class=\"vat-price\"><span class=\"vat-note-product-price\">";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("excl.VAT", array("name" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getVATName", array(), "method")))), "html", null, true);
                echo "</span>
    </li>
  ";
            }
        }
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/modules/CDev/VAT/price.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 13,  27 => 10,  24 => 9,  22 => 8,  19 => 7,);
    }
}
/* {##*/
/*  # Product price value*/
/*  #*/
/*  # @ListChild (list="product.plain_price", weight="20")*/
/*  # @ListChild (list="product.plain_price_only", weight="20")*/
/*  #}*/
/* */
/* {% if this.isVATApplicable() %}*/
/*   {% if this.isDisplayedPriceIncludingVAT() %}*/
/*     <li class="vat-price"><span class="vat-note-product-price">{{ t('incl.VAT', {'name': this.getVATName()}) }}</span>*/
/*     </li>*/
/*   {% else %}*/
/*     <li class="vat-price"><span class="vat-note-product-price">{{ t('excl.VAT', {'name': this.getVATName()}) }}</span>*/
/*     </li>*/
/*   {% endif %}*/
/* {% endif %}*/
/* */
