<?php

/* menu/info/top_menu_node.twig */
class __TwigTemplate_8ece7e06d6fdef1fa660d051d13e9ed8d60430cc6778a861b1f5c91630e3a970 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"info-menu menu notification\">
  <div class=\"icon\">
    ";
        // line 6
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getSVGImage", array(0 => "images/bell.svg"), "method");
        echo "
    <div class=\"unread-mark\"></div>
  </div>
  <div class=\"box\">";
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Menu\\Admin\\Info\\LazyLoad"))), "html", null, true);
        echo "</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "menu/info/top_menu_node.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 9,  23 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Info top menu node*/
/*  #}*/
/* <div class="info-menu menu notification">*/
/*   <div class="icon">*/
/*     {{ this.getSVGImage('images/bell.svg')|raw }}*/
/*     <div class="unread-mark"></div>*/
/*   </div>*/
/*   <div class="box">{{ widget('XLite\\View\\Menu\\Admin\\Info\\LazyLoad') }}</div>*/
/* </div>*/
/* */
