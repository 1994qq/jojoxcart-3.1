<?php

/* modules/XC/ThemeTweaker/themetweaker_panel/panel.twig */
class __TwigTemplate_cd8fa2844888d56a84a238066f8aca5f8c93730fb415ae0985849d12701258ca extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div id=\"themetweaker-panel-loader-point\">
  <xlite-themetweaker-panel inline-template mode=\"";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getThemeTweakerMode", array(), "method"), "html", null, true);
        echo "\">
    <div class=\"themetweaker-panel-wrapper themetweaker-panel--initial ";
        // line 7
        echo (($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getThemeTweakerMode", array(), "method")) ? ("state-on") : ("state-off"));
        echo "\">
      <div id=\"themetweaker-panel\" class=\"themetweaker-panel\" v-show=\"isExpanded\" transition=\"expand\">
        <div class=\"themetweaker-panel--inner\" :class=\"panelClasses\">
          <div class=\"themetweaker-panel--header\">
            ";
        // line 11
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "themetweaker-panel--header"))), "html", null, true);
        echo "
          </div>
          <div class=\"themetweaker-panel--body\" v-show=\"mode\">
            ";
        // line 14
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "themetweaker-panel--body"))), "html", null, true);
        echo "
          </div>
        </div>
      </div>
      ";
        // line 18
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "themetweaker-panel-extensions"))), "html", null, true);
        echo "
    </div>
  </xlite-themetweaker-panel>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/XC/ThemeTweaker/themetweaker_panel/panel.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 18,  40 => 14,  34 => 11,  27 => 7,  23 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Layout editor panel*/
/*  #}*/
/* */
/* <div id="themetweaker-panel-loader-point">*/
/*   <xlite-themetweaker-panel inline-template mode="{{ this.getThemeTweakerMode() }}">*/
/*     <div class="themetweaker-panel-wrapper themetweaker-panel--initial {{ this.getThemeTweakerMode() ? 'state-on' : 'state-off' }}">*/
/*       <div id="themetweaker-panel" class="themetweaker-panel" v-show="isExpanded" transition="expand">*/
/*         <div class="themetweaker-panel--inner" :class="panelClasses">*/
/*           <div class="themetweaker-panel--header">*/
/*             {{ widget_list('themetweaker-panel--header') }}*/
/*           </div>*/
/*           <div class="themetweaker-panel--body" v-show="mode">*/
/*             {{ widget_list('themetweaker-panel--body') }}*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*       {{ widget_list('themetweaker-panel-extensions') }}*/
/*     </div>*/
/*   </xlite-themetweaker-panel>*/
/* </div>*/
