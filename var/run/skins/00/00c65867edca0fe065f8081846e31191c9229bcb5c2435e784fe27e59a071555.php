<?php

/* /var/www/html/xcart/skins/admin/export/parts/popup.completed.message.twig */
class __TwigTemplate_5d9f311eb53dcfec24590fd46907baa37ab214a9d3f98c61189d4ce42847bc3a extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
 <p class=\"help\">";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("The download should start automatically. Click the link if does not start.")), "html", null, true);
        echo "</p>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/export/parts/popup.completed.message.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Export completed section : button*/
/*  #*/
/*  # @ListChild (list="export.popup.completed.content", weight="100")*/
/*  #}*/
/* */
/*  <p class="help">{{ t('The download should start automatically. Click the link if does not start.') }}</p>*/
