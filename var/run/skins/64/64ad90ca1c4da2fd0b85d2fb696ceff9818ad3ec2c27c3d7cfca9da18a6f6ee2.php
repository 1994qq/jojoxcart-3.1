<?php

/* product/details/parts/common.product-added.twig */
class __TwigTemplate_7e649fa008c1fe1779a9294bd0374de7643de6c4d575819031b3935128ec533f extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isProductAdded", array(), "method")) {
            // line 5
            echo "<p class=\"product-added-note\">
  <i class=\"icon-ok-mark\"></i>
</p>
";
        }
    }

    public function getTemplateName()
    {
        return "product/details/parts/common.product-added.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  21 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Product details buttons block*/
/*  #}*/
/* {% if this.isProductAdded() %}*/
/* <p class="product-added-note">*/
/*   <i class="icon-ok-mark"></i>*/
/* </p>*/
/* {% endif %}*/
/* */
