<?php

/* selected_attribute_values/body.twig */
class __TwigTemplate_86d0bb2674a05da733833882ec30524dbdb8758cae514274482b12e5309a1674 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"item-attribute-values\">
  <ul class=\"selected-attribute-values\">
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "item", array()), "getSortedAttributeValues", array(), "method"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
            // line 8
            echo "      <li>
        <span>";
            // line 9
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["option"], "getActualName", array(), "method"), "html", null, true);
            echo ":</span>
        ";
            // line 10
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["option"], "getActualValue", array(), "method"), "html", null, true);
            if (($this->getAttribute($context["loop"], "index", array()) != $this->getAttribute($context["loop"], "length", array()))) {
                echo ", ";
            }
            // line 11
            echo "      </li>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "  </ul>
  
  ";
        // line 15
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isChangeAttributesLinkVisible", array(), "method")) {
            // line 16
            echo "    <div class=\"item-change-attribute-values\">
      <a href=\"";
            // line 17
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getChangeAttributeValuesLink", array(), "method"), "html", null, true);
            echo "\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Change attributes")), "html", null, true);
            echo "</a>
    </div>
  ";
        }
        // line 20
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "selected_attribute_values/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 20,  77 => 17,  74 => 16,  72 => 15,  68 => 13,  53 => 11,  48 => 10,  44 => 9,  41 => 8,  24 => 7,  19 => 4,);
    }
}
/* {##*/
/*  # Selected attribute values*/
/*  #}*/
/* */
/* <div class="item-attribute-values">*/
/*   <ul class="selected-attribute-values">*/
/*     {% for option in this.item.getSortedAttributeValues() %}*/
/*       <li>*/
/*         <span>{{ option.getActualName() }}:</span>*/
/*         {{ option.getActualValue() }}{% if loop.index != loop.length %}, {% endif %}*/
/*       </li>*/
/*     {% endfor %}*/
/*   </ul>*/
/*   */
/*   {% if this.isChangeAttributesLinkVisible() %}*/
/*     <div class="item-change-attribute-values">*/
/*       <a href="{{ this.getChangeAttributeValuesLink() }}">{{ t('Change attributes') }}</a>*/
/*     </div>*/
/*   {% endif %}*/
/* </div>*/
/* */
