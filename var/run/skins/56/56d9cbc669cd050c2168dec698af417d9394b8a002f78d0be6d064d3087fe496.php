<?php

/* /var/www/html/xcart/skins/common/modules/CDev/VAT/order/invoice/item.rate.twig */
class __TwigTemplate_c3cc6549183861efe0dd02a926266007251a3c971e91e378b4789788314c6c61 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
";
        // line 7
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isUseVatInvoiceMode", array(), "method")) {
            // line 8
            echo "<td class=\"vat-rate\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "item", array()), "getVatRateString", array(), "method"), "html", null, true);
            echo "</td>
";
        }
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/common/modules/CDev/VAT/order/invoice/item.rate.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 8,  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Price item cell*/
/*  #*/
/*  # @ListChild (list="invoice.item", weight="17")*/
/*  #}*/
/* */
/* {% if this.isUseVatInvoiceMode() %}*/
/* <td class="vat-rate">{{ this.item.getVatRateString() }}</td>*/
/* {% endif %}*/
/* */
