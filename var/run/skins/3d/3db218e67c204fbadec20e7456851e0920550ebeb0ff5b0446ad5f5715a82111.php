<?php

/* /var/www/html/xcart/skins/admin/items_list/model/table/category/parts/info.subcategories.twig */
class __TwigTemplate_e3c481e05f54d3739ff4b49d03992529065cc31671a7bd90cc76fecb0bbecc1f extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<a href=\"";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('url')->getCallable(), array($this->env, $context, "categories", "", array("id" => $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "entity", array()), "getCategoryId", array(), "method")))), "html", null, true);
        echo "\" class=\"count-link\">";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "entity", array()), "getSubcategoriesCount", array(), "method"), "html", null, true);
        echo "</a>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/items_list/model/table/category/parts/info.subcategories.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Subcategories count link*/
/*  #*/
/*  # @ListChild (list="itemsList.category.cell.subcategories", weight="100")*/
/*  #}*/
/* */
/* <a href="{{ url('categories', '', {'id': this.entity.getCategoryId()}) }}" class="count-link">{{ this.entity.getSubcategoriesCount() }}</a>*/
/* */
