<?php

/* left_menu/body.twig */
class __TwigTemplate_eba8a49d4c9af2f3c79b966d5538b538e19f9bbceeecb01b25be4e4a5cecfc6e extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div ";
        // line 5
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "printTagAttributes", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getContainerTagAttributes", array(), "method")), "method");
        echo ">
  ";
        // line 6
        $context["items"] = $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getItems", array(), "method");
        // line 7
        echo "  ";
        if ((isset($context["items"]) ? $context["items"] : null)) {
            // line 8
            echo "    <ul class=\"menu main-menu\">
      ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 10
                echo "        ";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["item"], "display", array(), "method"), "html", null, true);
                echo "
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "    </ul>
  ";
        }
        // line 14
        echo "
  ";
        // line 15
        $context["bottomItems"] = $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getBottomItems", array(), "method");
        // line 16
        echo "  ";
        if ((isset($context["bottomItems"]) ? $context["bottomItems"] : null)) {
            // line 17
            echo "    <ul class=\"menu bottom-menu\">
      ";
            // line 18
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["bottomItems"]) ? $context["bottomItems"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
                // line 19
                echo "        ";
                echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["link"], "display", array(), "method"), "html", null, true);
                echo "
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "    </ul>
  ";
        }
        // line 23
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "left_menu/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 23,  75 => 21,  66 => 19,  62 => 18,  59 => 17,  56 => 16,  54 => 15,  51 => 14,  47 => 12,  38 => 10,  34 => 9,  31 => 8,  28 => 7,  26 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Left side menu*/
/*  #}*/
/* */
/* <div {{ this.printTagAttributes(this.getContainerTagAttributes())|raw }}>*/
/*   {% set items = this.getItems() %}*/
/*   {% if items %}*/
/*     <ul class="menu main-menu">*/
/*       {% for item in items %}*/
/*         {{ item.display() }}*/
/*       {% endfor %}*/
/*     </ul>*/
/*   {% endif %}*/
/* */
/*   {% set bottomItems = this.getBottomItems() %}*/
/*   {% if bottomItems %}*/
/*     <ul class="menu bottom-menu">*/
/*       {% for link in bottomItems %}*/
/*         {{ link.display() }}*/
/*       {% endfor %}*/
/*     </ul>*/
/*   {% endif %}*/
/* </div>*/
/* */
