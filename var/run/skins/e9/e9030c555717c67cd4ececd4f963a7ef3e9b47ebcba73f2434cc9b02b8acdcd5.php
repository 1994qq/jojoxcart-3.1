<?php

/* /var/www/html/xcart/skins/customer/mini_cart/horizontal/parts/item.name.twig */
class __TwigTemplate_0bc5ec92ffcbea9d0aee2e9c0c36260b4b0b9700e41b63eac41cef1ad26890a3 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<span class=\"item-name\"><a href=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "item", array()), "getURL", array(), "method"), "html", null, true);
        echo "\">";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "item", array()), "getName", array(), "method"), "html", null, true);
        echo "</a></span>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/mini_cart/horizontal/parts/item.name.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Display horizontal minicart item name*/
/*  #*/
/*  # @ListChild (list="minicart.horizontal.item", weight="10")*/
/*  #}*/
/* <span class="item-name"><a href="{{ this.item.getURL() }}">{{ this.item.getName() }}</a></span>*/
/* */
