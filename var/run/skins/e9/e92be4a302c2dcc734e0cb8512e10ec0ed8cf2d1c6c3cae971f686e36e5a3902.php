<?php

/* layout/header/orders.twig */
class __TwigTemplate_d69ec4bfd2fb8247002202cf193a29663e5928dc42d5a46906ccd440869a1c1a extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<li class=\"account-link-orders\">
  <a href=\"";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getOrdersListUrl", array(), "method"), "html", null, true);
        echo "\" class=\"orders icon-orders\">
  \t<span>";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getCaption", array(), "method"), "html", null, true);
        echo "</span>
  </a>
</li>
";
    }

    public function getTemplateName()
    {
        return "layout/header/orders.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 7,  23 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Orders list*/
/*  #}*/
/* */
/* <li class="account-link-orders">*/
/*   <a href="{{ this.getOrdersListUrl() }}" class="orders icon-orders">*/
/*   	<span>{{ this.getCaption() }}</span>*/
/*   </a>*/
/* </li>*/
/* */
