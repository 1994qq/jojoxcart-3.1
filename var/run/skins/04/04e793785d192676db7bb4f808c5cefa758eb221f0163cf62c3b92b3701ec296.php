<?php

/* /var/www/html/xcart/skins/admin/layout_settings/parts/layout_settings.settings.twig */
class __TwigTemplate_20a880bf7c12b33aa70ce968b86d5704148e0fbdd2df6d6460b16723bf10ad4a extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\LayoutSettings\\Settings"))), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/layout_settings/parts/layout_settings.settings.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Layout settings template*/
/*  #*/
/*  # @ListChild (list="layout_settings", weight="10")*/
/*  #}*/
/* {{ widget('XLite\\View\\LayoutSettings\\Settings') }}*/
/* */
