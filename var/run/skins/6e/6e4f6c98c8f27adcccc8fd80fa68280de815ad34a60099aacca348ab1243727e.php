<?php

/* modules/XC/ThemeTweaker/themetweaker_panel/modal/modal.twig */
class __TwigTemplate_3a08346732672f92c3d27e8dbcdc1c7c464b7ea926b1d5ba2cb987fbaf905762 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<script type=\"x/template\" id=\"themetweaker-modal-template\">
  <div class=\"themetweaker-modal-mask\" v-show=\"show\" transition=\"modal\" :data-namespace=\"namespace\">
    <div class=\"themetweaker-modal-wrapper\">
      <div class=\"themetweaker-modal-container\">

        <div class=\"themetweaker-modal-header\">
          <slot name=\"header\">
            <a class=\"themetweaker-modal-close\"
                    @click=\"sendEvent('cancel')\">
              ";
        // line 14
        echo call_user_func_array($this->env->getFunction('svg')->getCallable(), array($this->env, $context, "images/close.svg", "common"));
        echo "
            </a>
          </slot>
        </div>

        <div class=\"themetweaker-modal-body\">
          <slot name=\"body\">
          </slot>
        </div>

        <div class=\"themetweaker-modal-footer\">
          <slot name=\"footer\">
            <button class=\"themetweaker-modal-button secondary\"
                    @click=\"sendEvent('cancel')\">
              <span>";
        // line 28
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Cancel")), "html", null, true);
        echo "</span>
            </button>
            <button class=\"themetweaker-modal-button\"
                    @click=\"sendEvent('ok')\">
              <span>";
        // line 32
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("OK")), "html", null, true);
        echo "</span>
            </button>
          </slot>
        </div>
      </div>
    </div>
  </div>
</script>
";
    }

    public function getTemplateName()
    {
        return "modules/XC/ThemeTweaker/themetweaker_panel/modal/modal.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 32,  48 => 28,  31 => 14,  19 => 4,);
    }
}
/* {##*/
/*  # Layout editor panel*/
/*  #}*/
/* */
/* <script type="x/template" id="themetweaker-modal-template">*/
/*   <div class="themetweaker-modal-mask" v-show="show" transition="modal" :data-namespace="namespace">*/
/*     <div class="themetweaker-modal-wrapper">*/
/*       <div class="themetweaker-modal-container">*/
/* */
/*         <div class="themetweaker-modal-header">*/
/*           <slot name="header">*/
/*             <a class="themetweaker-modal-close"*/
/*                     @click="sendEvent('cancel')">*/
/*               {{ svg('images/close.svg', 'common')|raw }}*/
/*             </a>*/
/*           </slot>*/
/*         </div>*/
/* */
/*         <div class="themetweaker-modal-body">*/
/*           <slot name="body">*/
/*           </slot>*/
/*         </div>*/
/* */
/*         <div class="themetweaker-modal-footer">*/
/*           <slot name="footer">*/
/*             <button class="themetweaker-modal-button secondary"*/
/*                     @click="sendEvent('cancel')">*/
/*               <span>{{ t('Cancel') }}</span>*/
/*             </button>*/
/*             <button class="themetweaker-modal-button"*/
/*                     @click="sendEvent('ok')">*/
/*               <span>{{ t('OK') }}</span>*/
/*             </button>*/
/*           </slot>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* </script>*/
/* */
