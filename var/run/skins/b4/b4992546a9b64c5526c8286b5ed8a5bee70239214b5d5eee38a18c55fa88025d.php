<?php

/* items_list/product/parts/common.added-mark.twig */
class __TwigTemplate_96a5e752014dc6369ae7e909456fd7cb09186212c975c793f3060ebd98460adf extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<div title=\"Added to cart\" class=\"added-to-cart\"><i class=\"icon-ok-mark\"></i></div>
";
    }

    public function getTemplateName()
    {
        return "items_list/product/parts/common.added-mark.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 4,);
    }
}
/* {##*/
/*  # Added-to-cart mark*/
/*  #}*/
/* <div title="Added to cart" class="added-to-cart"><i class="icon-ok-mark"></i></div>*/
/* */
