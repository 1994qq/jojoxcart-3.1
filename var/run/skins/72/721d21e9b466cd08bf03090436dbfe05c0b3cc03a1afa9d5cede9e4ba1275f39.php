<?php

/* /var/www/html/xcart/skins/admin/items_list/product/modify/common/parts/header/checkbox.twig */
class __TwigTemplate_2b2f9d8a4a3a32b2715da839ca814dac1e26e55a7a321c77604df61a9152014f extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<th class=\"checkboxes\"><input type=\"checkbox\" class=\"column-selector\" /></th>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/items_list/product/modify/common/parts/header/checkbox.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Column with checkboxes*/
/*  #*/
/*  # @ListChild (list="itemsList.product.modify.common.admin.header", weight="10")*/
/*  #}*/
/* */
/* <th class="checkboxes"><input type="checkbox" class="column-selector" /></th>*/
/* */
