<?php

/* /var/www/html/xcart/skins/customer/header/parts/title.twig */
class __TwigTemplate_5a03cc008351cab2134ea8083ba2728d262d5b437a66a189c3168dd49bd72f8f extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<title>";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getPageTitle", array(), "method"), "html", null, true);
        echo "</title>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/header/parts/title.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Head list children*/
/*  #*/
/*  # @ListChild (list="head", weight="100")*/
/*  #}*/
/* */
/* <title>{{ this.getPageTitle() }}</title>*/
/* */
