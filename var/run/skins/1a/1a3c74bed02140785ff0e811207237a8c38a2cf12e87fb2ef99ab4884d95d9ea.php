<?php

/* /var/www/html/xcart/skins/admin/browse_server/parts/buttons.twig */
class __TwigTemplate_5fdab0e29c4940c980c10c6c44a62a3c3a815cc11e03e8652d24d2a7926ffa07 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<div class=\"browse-selector-actions\">
  <button class=\"back-button\">";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Back to file select")), "html", null, true);
        echo "</button>
  <button class=\"choose-file-button main-button\">";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Choose file")), "html", null, true);
        echo "</button>
</div>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/browse_server/parts/buttons.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 8,  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Browser server dialog : buttons*/
/*  #*/
/*  # @ListChild (list="browseServer", zone="admin", weight="200")*/
/*  #}*/
/* <div class="browse-selector-actions">*/
/*   <button class="back-button">{{ t('Back to file select') }}</button>*/
/*   <button class="choose-file-button main-button">{{ t('Choose file') }}</button>*/
/* </div>*/
/* */
