<?php

/* /var/www/html/xcart/skins/customer/product/details/parts/common.price.twig */
class __TwigTemplate_fc648d875509da6e2243ae04f1421023b803ebe11023681c52101ad623ec84c3 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Price", "product" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "product", array())))), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/product/details/parts/common.price.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 7,);
    }
}
/* {##*/
/*  # Product price*/
/*  #*/
/*  # @ListChild (list="product.details.page.info", weight="40")*/
/*  # @ListChild (list="product.details.quicklook.info", weight="40")*/
/*  #}*/
/* {{ widget('\\XLite\\View\\Price', product=this.product) }}*/
/* */
