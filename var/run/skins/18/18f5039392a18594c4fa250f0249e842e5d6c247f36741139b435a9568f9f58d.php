<?php

/* /var/www/html/xcart/skins/customer/items_list/product/parts/table.captions.price.twig */
class __TwigTemplate_ba2a613e1a1d0d7a60fd3526d6756c25146d72aead014ef35546bf0480e03120 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<th class=\"caption-product-price\">";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Price")), "html", null, true);
        echo "</th>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/items_list/product/parts/table.captions.price.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Item price*/
/*  #*/
/*  # @ListChild (list="itemsList.product.table.customer.captions", weight="40")*/
/*  #}*/
/* <th class="caption-product-price">{{ t('Price') }}</th>*/
/* */
