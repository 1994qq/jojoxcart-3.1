<?php

/* common/surcharge.twig */
class __TwigTemplate_5006dec277684073e06212f77ad66b0a2e6521482ae97231f96bec0948db1a86 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<span class=\"surcharge\">
  ";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "surcharge.common"))), "html", null, true);
        echo "
</span>
";
    }

    public function getTemplateName()
    {
        return "common/surcharge.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Surcharge widget*/
/*  #}*/
/* */
/* <span class="surcharge">*/
/*   {{ widget_list('surcharge.common') }}*/
/* </span>*/
/* */
