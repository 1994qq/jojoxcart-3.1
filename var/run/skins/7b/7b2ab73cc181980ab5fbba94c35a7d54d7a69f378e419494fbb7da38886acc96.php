<?php

/* /var/www/html/xcart/skins/admin/upgrade/step/prepare/entries_list_upgrade/parts/table/info/module.twig */
class __TwigTemplate_7fefc3619cbce3e19570eb5e82304e087e91efa6a25466a1d90997452961de80 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<li class=\"module\">
  <ul class=\"details\">
  ";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "sections.table.info.module", "type" => "inherited", "entry" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "entry", array())))), "html", null, true);
        echo "
  </ul>
</li>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/upgrade/step/prepare/entries_list_upgrade/parts/table/info/module.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 8,  19 => 6,);
    }
}
/* {##*/
/*  # Upgrade entry icon*/
/*  #*/
/*  # @ListChild (list="upgrade.step.prepare.entries_list_upgrade.sections.table.info", weight="200")*/
/*  #}*/
/* <li class="module">*/
/*   <ul class="details">*/
/*   {{ widget_list('sections.table.info.module', type='inherited', entry=this.entry) }}*/
/*   </ul>*/
/* </li>*/
/* */
