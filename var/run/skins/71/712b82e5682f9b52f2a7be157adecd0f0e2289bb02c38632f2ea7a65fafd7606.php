<?php

/* /var/www/html/xcart/skins/admin/upgrade/step/ready_to_install/entries_list/parts/table/header/status.twig */
class __TwigTemplate_33ab22cbfac82eb0bd0c196e169825011298af32b960440cbeb2556bebc36c1b extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 7
        echo "
<th class=\"status\">";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Status")), "html", null, true);
        echo "</th>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/upgrade/step/ready_to_install/entries_list/parts/table/header/status.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 8,  19 => 7,);
    }
}
/* {##*/
/*  # The "Status" column header*/
/*  #*/
/*  # @ListChild (list="upgrade.step.ready_to_install.entries_list.sections.table.header", weight="200")*/
/*  # @ListChild (list="upgrade.step.completed.entries_list.sections.table.header", weight="200")*/
/*  #}*/
/* */
/* <th class="status">{{ t('Status') }}</th>*/
/* */
