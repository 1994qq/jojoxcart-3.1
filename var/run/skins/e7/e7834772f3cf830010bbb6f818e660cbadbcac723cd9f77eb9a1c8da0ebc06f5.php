<?php

/* /var/www/html/xcart/skins/admin/order/packing_slip/parts/footer/footer.additional.twig */
class __TwigTemplate_78997f98cef4398d44fc193fd54650e5975bc13fa9fe32afa510082e8e7d9326 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<div class=\"thank-you\">";
        echo call_user_func_array($this->env->getFunction('t')->getCallable(), array("Thank you for your order FOOTER"));
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/order/packing_slip/parts/footer/footer.additional.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Order customer note*/
/*  #*/
/*  # @ListChild (list="packing_slip.footer", weight="20")*/
/*  #}*/
/* <div class="thank-you">{{ t('Thank you for your order FOOTER')|raw }}</div>*/
/* */
