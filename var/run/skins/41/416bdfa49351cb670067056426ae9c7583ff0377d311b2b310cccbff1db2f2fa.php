<?php

/* /var/www/html/xcart/skins/customer/items_list/product/parts/common.button-buy-selected.twig */
class __TwigTemplate_5323547181223ab53bbbe7d22494654fd570de8f73e7fa5643993afdfed2819a extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<!--widget class=\"\\XLite\\View\\Button\\Submit\" style=\"product-buy-selected\" label=\"Buy selected\" /-->
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/items_list/product/parts/common.button-buy-selected.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Item buttons*/
/*  #*/
/*  # @ListChild (list="itemsList.product.table.customer.buttons", weight="50")*/
/*  #}*/
/* <!--widget class="\XLite\View\Button\Submit" style="product-buy-selected" label="Buy selected" /-->*/
/* */
