<?php

/* modules/XC/ThemeTweaker/button/themetweaker-tab.twig */
class __TwigTemplate_3dad87d1d353695de8a178c85c3f8355e4eb548af40a0b30ba360bcc1856f28d extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div ";
        // line 5
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "printTagAttributes", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getWrapperAttributes", array(), "method")), "method");
        echo ">
<button ";
        // line 6
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "printTagAttributes", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getAttributes", array(), "method")), "method");
        echo ">
  ";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "displayCommentedData", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getCommentedData", array(), "method")), "method"), "html", null, true);
        echo "
  ";
        // line 8
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getSvgIcon", array(), "method")) {
            // line 9
            echo "    ";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "displaySVGImage", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getSvgIcon", array(), "method")), "method"), "html", null, true);
            echo "
  ";
        }
        // line 11
        echo "  <span>";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getButtonLabel", array(), "method"))), "html", null, true);
        echo "</span>
</button>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/XC/ThemeTweaker/button/themetweaker-tab.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 11,  36 => 9,  34 => 8,  30 => 7,  26 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Regular button*/
/*  #}*/
/* */
/* <div {{ this.printTagAttributes(this.getWrapperAttributes())|raw }}>*/
/* <button {{ this.printTagAttributes(this.getAttributes())|raw }}>*/
/*   {{ this.displayCommentedData(this.getCommentedData()) }}*/
/*   {% if this.getSvgIcon() %}*/
/*     {{ this.displaySVGImage(this.getSvgIcon()) }}*/
/*   {% endif %}*/
/*   <span>{{ t(this.getButtonLabel()) }}</span>*/
/* </button>*/
/* </div>*/
