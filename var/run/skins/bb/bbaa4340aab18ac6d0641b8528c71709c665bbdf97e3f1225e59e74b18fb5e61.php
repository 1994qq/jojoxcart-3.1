<?php

/* shopping_cart/parts/box.estimator.method.twig */
class __TwigTemplate_a991c615d822181ec6b0b47d1b98e2c3996b082324dab5eb9ee1228f7490099d extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"selected-shipping-method\">
  <span class=\"method-name\">";
        // line 5
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getName", array(), "method");
        echo "</span>
  <span class=\"shipping-cost\">(";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getCost", array(), "method"), "html", null, true);
        echo ")</span>
</div>
";
    }

    public function getTemplateName()
    {
        return "shopping_cart/parts/box.estimator.method.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Shipping estimator box (selected method)*/
/*  #}*/
/* <div class="selected-shipping-method">*/
/*   <span class="method-name">{{ this.getName()|raw }}</span>*/
/*   <span class="shipping-cost">({{ this.getCost() }})</span>*/
/* </div>*/
/* */
