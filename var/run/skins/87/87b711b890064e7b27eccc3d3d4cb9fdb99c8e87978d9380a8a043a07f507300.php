<?php

/* mini_cart/horizontal/parts/item.attribute_values.twig */
class __TwigTemplate_1ac67d6e9d9c4bdc4720310798f440d1275c2ad581da7daec3a08012b64af058 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<a class=\"item-attribute-values underline-emulation\" id=\"item-attribute";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "item", array()), "getItemId", array(), "method"), "html", null, true);
        echo "\" data-rel=\"div.item-attribute-values.item-";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "item", array()), "getItemId", array(), "method"), "html", null, true);
        echo "\">
  <span>";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("attributes")), "html", null, true);
        echo "</span>
</a>
<div class=\"internal-popup item-attribute-values item-";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "item", array()), "getItemId", array(), "method"), "html", null, true);
        echo "\" style=\"display: none;\">
  <ul class=\"item-attribute-values\">
    ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "item", array()), "getSortedAttributeValues", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getMaxAttributesCount", array(), "method")), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["av"]) {
            // line 11
            echo "      <li>";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["av"], "getActualName", array(), "method"), "html", null, true);
            echo ": ";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["av"], "getActualValue", array(), "method"), "html", null, true);
            echo "</li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['av'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "  </ul>
  ";
        // line 14
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "needMoreAttributesLink", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "item", array())), "method")) {
            // line 15
            echo "    <div class=\"more-attributes\">
        <a href=\"";
            // line 16
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('url')->getCallable(), array($this->env, $context, "cart")), "html", null, true);
            echo "\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("More attributes")), "html", null, true);
            echo "</a>
    </div>
  ";
        }
        // line 19
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "mini_cart/horizontal/parts/item.attribute_values.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 19,  61 => 16,  58 => 15,  56 => 14,  53 => 13,  42 => 11,  38 => 10,  33 => 8,  28 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Minicart row with item attribute-values*/
/*  #}*/
/* */
/* <a class="item-attribute-values underline-emulation" id="item-attribute{{ this.item.getItemId() }}" data-rel="div.item-attribute-values.item-{{ this.item.getItemId() }}">*/
/*   <span>{{ t('attributes') }}</span>*/
/* </a>*/
/* <div class="internal-popup item-attribute-values item-{{ this.item.getItemId() }}" style="display: none;">*/
/*   <ul class="item-attribute-values">*/
/*     {% for av in this.item.getSortedAttributeValues(this.getMaxAttributesCount()) %}*/
/*       <li>{{ av.getActualName() }}: {{ av.getActualValue() }}</li>*/
/*     {% endfor %}*/
/*   </ul>*/
/*   {% if this.needMoreAttributesLink(this.item) %}*/
/*     <div class="more-attributes">*/
/*         <a href="{{ url('cart') }}">{{ t('More attributes') }}</a>*/
/*     </div>*/
/*   {% endif %}*/
/* </div>*/
/* */
