<?php

/* modules/XC/ThemeTweaker/themetweaker/webmaster_mode/template_code.twig */
class __TwigTemplate_a3270b8d95dc7ce38ff14f6753a4e6e29855cbe233eb328b9bfa6d798f68496e extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<xlite-template-code inline-template :interface=\"interface\" :template=\"template\">
  <div class=\"xlite-template-code\" :class=\"classes\">
    ";
        // line 7
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getTemplatePath", array(), "method")) {
            // line 8
            echo "      ";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\Module\\XC\\ThemeTweaker\\View\\FormField\\Textarea\\CodeMirror", "attributes" => array("v-pre" => "v-pre", "data-template-editor" => "data-template-editor"), "fieldOnly" => true, "formControl" => false, "codeMode" => "twig"))), "html", null, true);
            // line 17
            echo "
      <script type=\"text/plain\" data-template-content>";
            // line 18
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getTemplateContent", array(), "method"), "html", null, true);
            echo "</script>
    ";
        }
        // line 20
        echo "  </div>
</xlite-template-code>";
    }

    public function getTemplateName()
    {
        return "modules/XC/ThemeTweaker/themetweaker/webmaster_mode/template_code.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 20,  32 => 18,  29 => 17,  26 => 8,  24 => 7,  19 => 4,);
    }
}
/* {##*/
/*  # Layout editor panel*/
/*  #}*/
/* */
/* <xlite-template-code inline-template :interface="interface" :template="template">*/
/*   <div class="xlite-template-code" :class="classes">*/
/*     {% if this.getTemplatePath() %}*/
/*       {{ widget(*/
/*         'XLite\\Module\\XC\\ThemeTweaker\\View\\FormField\\Textarea\\CodeMirror',*/
/*         attributes={*/
/*           'v-pre': 'v-pre',*/
/*           'data-template-editor': 'data-template-editor',*/
/*         },*/
/*         fieldOnly=true,*/
/*         formControl=false,*/
/*         codeMode='twig'*/
/*       ) }}*/
/*       <script type="text/plain" data-template-content>{{ this.getTemplateContent() }}</script>*/
/*     {% endif %}*/
/*   </div>*/
/* </xlite-template-code>*/
