<?php

/* /var/www/html/xcart/skins/common/modules/CDev/VAT/order/invoice/head.companyAddress.merchantId.twig */
class __TwigTemplate_6db70afd3ddfed4ec110956917f95cb24d03c08415a1145e5077abb63d248fb4 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
";
        // line 7
        if (($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isUseVatInvoiceMode", array(), "method") && $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "config", array()), "CDev", array()), "VAT", array()), "merchant_tax_number", array()))) {
            // line 8
            echo "<p class=\"vat-merchant-number\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Merchant Tax Number (VAT ID, ABN etc)")), "html", null, true);
            echo ": ";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "config", array()), "CDev", array()), "VAT", array()), "merchant_tax_number", array()), "html", null, true);
            echo "</p>
";
        }
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/common/modules/CDev/VAT/order/invoice/head.companyAddress.merchantId.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 8,  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Invoice : header : Company address box : Merchant tax number*/
/*  #*/
/*  # @ListChild (list="invoice.subhead.companyAddress", weight="600")*/
/*  #}*/
/* */
/* {% if this.isUseVatInvoiceMode() and this.config.CDev.VAT.merchant_tax_number %}*/
/* <p class="vat-merchant-number">{{ t('Merchant Tax Number (VAT ID, ABN etc)') }}: {{ this.config.CDev.VAT.merchant_tax_number }}</p>*/
/* {% endif %}*/
/* */
