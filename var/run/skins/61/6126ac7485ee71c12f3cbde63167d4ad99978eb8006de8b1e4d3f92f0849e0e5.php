<?php

/* /var/www/html/xcart/skins/admin/upgrade/step/prepare/entries_list_update/parts/table/header/component.twig */
class __TwigTemplate_b2f7ef3ca9605b5239470923fd6d1482ed4187dd0cfd025e2e3be373a7a19437 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<th colspan=\"2\" class=\"module-info-header\">";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Component")), "html", null, true);
        echo "</th>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/upgrade/step/prepare/entries_list_update/parts/table/header/component.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # The "Component" column header*/
/*  #*/
/*  # @ListChild (list="upgrade.step.prepare.entries_list_update.sections.table.header", weight="100")*/
/*  #}*/
/* */
/* <th colspan="2" class="module-info-header">{{ t('Component') }}</th>*/
/* */
