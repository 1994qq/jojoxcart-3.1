<?php

/* /var/www/html/xcart/skins/customer/items_list/product/parts/table.captions.sku.twig */
class __TwigTemplate_b3647913028fbacece038be48399ce68ac4a2e77b88ad29fa59dac0f28f2d72d extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<th class=\"caption-product-sku\">";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("SKU")), "html", null, true);
        echo "</th>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/items_list/product/parts/table.captions.sku.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Item SKU*/
/*  #*/
/*  # @ListChild (list="itemsList.product.table.customer.captions", weight="10")*/
/*  #}*/
/* <th class="caption-product-sku">{{ t('SKU') }}</th>*/
/* */
