<?php

/* layout/header/mobile_header_parts/slidebar_menu.twig */
class __TwigTemplate_7d0ec70418b728b050e6bb098edc905592c418ca8557fc2ec63ce886f28a710b extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<li class=\"dropdown mobile_header-slidebar\">
  <a id=\"main_menu\" href=\"#slidebar\" class=\"icon-menu\"></a>
</li>";
    }

    public function getTemplateName()
    {
        return "layout/header/mobile_header_parts/slidebar_menu.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 4,);
    }
}
/* {##*/
/*  # Header right box*/
/*  #}*/
/* */
/* <li class="dropdown mobile_header-slidebar">*/
/*   <a id="main_menu" href="#slidebar" class="icon-menu"></a>*/
/* </li>*/
