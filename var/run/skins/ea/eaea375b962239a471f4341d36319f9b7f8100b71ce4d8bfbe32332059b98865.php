<?php

/* /var/www/html/xcart/skins/crisp_white/customer/shopping_cart/parts/total.separator.twig */
class __TwigTemplate_1ce3d0c78393a99a82448667d7d2fc74c122d8aaef7228f1895732915fbf4cdc extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<li class=\"separator\"></li>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/crisp_white/customer/shopping_cart/parts/total.separator.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Shopping cart subtotal*/
/*  #*/
/*  # @ListChild (list="cart.panel.totals", weight="25")*/
/*  #}*/
/* <li class="separator"></li>*/
/* */
