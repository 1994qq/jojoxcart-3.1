<?php

/* /var/www/html/xcart/skins/customer/modules/XC/ThemeTweaker/themetweaker_panel/panel/tabs.hide.twig */
class __TwigTemplate_a44edb467311582b771456a7fa04aab64059d3a7d359851dac50a536bfb90de1 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "XLite\\View\\Button\\Simple", "label" => "", "style" => "themetweaker-tab themetweaker-tab_hide themetweaker-close-icon", "attributes" => array("@click" => "hidePanel")))), "html", null, true);
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/modules/XC/ThemeTweaker/themetweaker_panel/panel/tabs.hide.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Layout editor panel*/
/*  #*/
/*  # @ListChild(list="themetweaker-panel--tabs", weight="0")*/
/*  #}*/
/* */
/* {{ widget('XLite\\View\\Button\\Simple',*/
/*   label="",*/
/*   style="themetweaker-tab themetweaker-tab_hide themetweaker-close-icon",*/
/*   attributes={"@click": "hidePanel"}) }}*/
