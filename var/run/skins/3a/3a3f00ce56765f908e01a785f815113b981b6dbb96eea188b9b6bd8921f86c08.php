<?php

/* /var/www/html/xcart/skins/common/modules/CDev/VAT/order/invoice/items.head.rate.twig */
class __TwigTemplate_63e3012abcdd28b0b49e482098460ebb6ca3e3dae0483504b10a32ea9cf003ec extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
";
        // line 7
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isUseVatInvoiceMode", array(), "method")) {
            // line 8
            echo "<th class=\"vat-rate\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getVatName", array(), "method"), "html", null, true);
            echo ", ";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("rate")), "html", null, true);
            echo "</th>
";
        }
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/common/modules/CDev/VAT/order/invoice/items.head.rate.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 8,  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Invoice : header : Company address box : Merchant tax number*/
/*  #*/
/*  # @ListChild (list="invoice.items.head", weight="17")*/
/*  #}*/
/* */
/* {% if this.isUseVatInvoiceMode() %}*/
/* <th class="vat-rate">{{ this.getVatName() }}, {{ t('rate') }}</th>*/
/* {% endif %}*/
/* */
