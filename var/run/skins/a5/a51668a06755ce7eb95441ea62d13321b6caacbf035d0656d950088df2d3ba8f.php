<?php

/* /var/www/html/xcart/skins/customer/operate_as_user/parts/login.twig */
class __TwigTemplate_61af93e051b1200ac27b9f72c2a76ec0eb94bc9eaa3d925c7e3c8dad999a1ad2 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<div class='login'>";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLogin", array(), "method"), "html", null, true);
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/operate_as_user/parts/login.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Login*/
/*  #*/
/*  # @ListChild (list="operate_as_user", weight="30")*/
/*  #}*/
/* */
/* <div class='login'>{{ this.getLogin() }}</div>*/
/* */
