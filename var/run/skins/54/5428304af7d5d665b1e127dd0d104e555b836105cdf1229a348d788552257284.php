<?php

/* /var/www/html/xcart/skins/customer/operate_as_user/parts/desc.twig */
class __TwigTemplate_972290d6ff98a15c23b4a5e899bcdbebf88f78d8ac618de9be87f4ae016e34a6 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<div class='description'>";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("You are operating as:")), "html", null, true);
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/operate_as_user/parts/desc.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Description*/
/*  #*/
/*  # @ListChild (list="operate_as_user", weight="10")*/
/*  #}*/
/* */
/* <div class='description'>{{ t('You are operating as:') }}</div>*/
/* */
