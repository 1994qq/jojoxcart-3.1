<?php

/* /var/www/html/xcart/skins/customer/items_list/product/parts/table.captions.qty.twig */
class __TwigTemplate_7ff501d17a81a4f698bc0894a7ac853563750a20ddb048f06fd65e2e933980ef extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<th class=\"caption-product-qty\"><!--Qty.--></th>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/items_list/product/parts/table.captions.qty.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Item SKU*/
/*  #*/
/*  # @ListChild (list="itemsList.product.table.customer.captions", weight="30")*/
/*  #}*/
/* <th class="caption-product-qty"><!--Qty.--></th>*/
/* */
