<?php

/* /var/www/html/xcart/skins/admin/items_list/module/sales_channels/items_list/clear.twig */
class __TwigTemplate_b9c2596991d17f491268572c54c92fdb965af7fdd31dfdf45b4fd94a40c9de5c extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<div class=\"clear\"></div>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/items_list/module/sales_channels/items_list/clear.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Clear div*/
/*  #*/
/*  # @ListChild (list="sales-channels.top-controls", weight="300")*/
/*  #}*/
/* <div class="clear"></div>*/
/* */
