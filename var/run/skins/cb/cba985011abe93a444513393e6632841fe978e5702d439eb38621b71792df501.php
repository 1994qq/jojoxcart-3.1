<?php

/* modules/qq/EditDesignButton/EditDesignButton.twig */
class __TwigTemplate_aa714efed1a15d86b48a38befdf4a51208069a9cc40dc1a401e44830795a7726 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form method=\"get\" action=\"http://13.76.134.147/\">
\t
\t<input type=\"hidden\" name=\"product_id\" value=\"";
        // line 3
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "item", array()), "getItemId", array(), "method"), "html", null, true);
        echo "\">
\t<input type=\"hidden\" name=\"design_id\" value=\"";
        // line 4
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "item", array()), "getDesignId", array(), "method"), "html", null, true);
        echo "\">

  \t<a href=\"http://13.76.134.147/\">
      \t\t<button type=\"submit\">Edit Your Design</button>
 \t</a>
</form>";
    }

    public function getTemplateName()
    {
        return "modules/qq/EditDesignButton/EditDesignButton.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  23 => 3,  19 => 1,);
    }
}
/* <form method="get" action="http://13.76.134.147/">*/
/* 	*/
/* 	<input type="hidden" name="product_id" value="{{ this.item.getItemId() }}">*/
/* 	<input type="hidden" name="design_id" value="{{ this.item.getDesignId() }}">*/
/* */
/*   	<a href="http://13.76.134.147/">*/
/*       		<button type="submit">Edit Your Design</button>*/
/*  	</a>*/
/* </form>*/
