<?php

/* /var/www/html/xcart/skins/admin/items_list/product/table/parts/header/position.twig */
class __TwigTemplate_85eacabdcf1d5c51f48e50f53b82251beeda966fb9cbf68cbf2343df36fb44a1 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<th class=\"pos\">";
        // line 7
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array("template" => "items_list/sort.twig", "title" => call_user_func_array($this->env->getFunction('t')->getCallable(), array("Pos.")), "sortByColumn" => ""))), "html", null, true);
        echo "</th>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/items_list/product/table/parts/header/position.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 7,  19 => 6,);
    }
}
/* {##*/
/*  # Item position*/
/*  #*/
/*  # @ListChild (list="itemsList.product.table.admin.search.header", weight="50")*/
/*  #}*/
/* */
/* <th class="pos">{{ widget(template='items_list/sort.twig', title=t('Pos.'), sortByColumn='') }}</th>*/
/* */
