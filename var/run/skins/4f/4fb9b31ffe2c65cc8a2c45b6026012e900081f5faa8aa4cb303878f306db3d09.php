<?php

/* layout/content/product.location.twig */
class __TwigTemplate_1fd63e459b180bd6f5804afa90ad3b1c0520a77a4b4bf3e5f0692721f1e5858d extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<div id=\"breadcrumb\">
    ";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "layout.main.breadcrumb"))), "html", null, true);
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "layout/content/product.location.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Location (breadcrumbs) for product page*/
/*  #}*/
/* <div id="breadcrumb">*/
/*     {{ widget_list('layout.main.breadcrumb') }}*/
/* </div>*/
/* */
