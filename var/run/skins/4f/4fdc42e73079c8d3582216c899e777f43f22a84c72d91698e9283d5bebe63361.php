<?php

/* /var/www/html/xcart/skins/admin/items_list/product/table/parts/header/checkbox.twig */
class __TwigTemplate_f2a32758ec641781ee246618426d40929c65fbb7d3048f89ca71dc1ff10970d5 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<th class=\"checkboxes\"><input type=\"checkbox\" class=\"check-all\" /></th>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/items_list/product/table/parts/header/checkbox.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Column with checkboxes*/
/*  #*/
/*  # @ListChild (list="itemsList.product.table.admin.search.header", weight="10")*/
/*  #}*/
/* */
/* <th class="checkboxes"><input type="checkbox" class="check-all" /></th>*/
/* */
