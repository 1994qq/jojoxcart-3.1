<?php

/* left_menu/extensions/link.twig */
class __TwigTemplate_85b1c237c06a75c65d23f66e662afb8406286402527c3165221c016823f455a0 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"line";
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLabel", array(), "method")) {
            echo " with-label";
        }
        echo "\">
  <a href=\"";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLink", array(), "method"), "html", null, true);
        echo "\" class=\"link\"";
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getTooltip", array(), "method")) {
            echo " title=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getTooltip", array(), "method"), "html", null, true);
            echo "\"";
        }
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getBlankPage", array(), "method")) {
            echo " target=\"_blank\"";
        }
        echo "><span class=\"icon\">";
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getIcon", array(), "method");
        echo "</span>";
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getTitle", array(), "method")) {
            echo "<span class=\"title\">";
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getTitle", array(), "method");
            echo "</span>";
        }
        echo "</a>

  ";
        // line 7
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getActionWidget", array(), "method")) {
            // line 8
            echo "    <div class=\"action-widget\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getActionWidget", array(), "method"), "display", array(), "method"), "html", null, true);
            echo "</div>
  ";
        }
        // line 10
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "left_menu/extensions/link.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 10,  50 => 8,  48 => 7,  26 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Left side menu link*/
/*  #}*/
/* <div class="line{% if this.getLabel() %} with-label{% endif %}">*/
/*   <a href="{{ this.getLink() }}" class="link"{% if this.getTooltip() %} title="{{ this.getTooltip() }}"{% endif %}{% if this.getBlankPage() %} target="_blank"{% endif %}><span class="icon">{{ this.getIcon()|raw }}</span>{% if this.getTitle() %}<span class="title">{{ this.getTitle()|raw }}</span>{% endif %}</a>*/
/* */
/*   {% if this.getActionWidget() %}*/
/*     <div class="action-widget">{{ this.getActionWidget().display() }}</div>*/
/*   {% endif %}*/
/* </div>*/
/* */
