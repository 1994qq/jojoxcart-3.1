<?php

/* main_center/page_container_parts/header_parts/storefront_status.twig */
class __TwigTemplate_8ccff57a6969a147c14c827706ad6cc23214221115be40bb1c5af7dd6b915411 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div ";
        // line 5
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "printTagAttributes", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getContainerTagAttributes", array(), "method")), "method");
        echo ">
  ";
        // line 6
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isTogglerVisible", array(), "method")) {
            // line 7
            echo "    <a href=\"";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLink", array(), "method"), "html", null, true);
            echo "\" ";
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "printTagAttributes", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getTogglerTagAttributes", array(), "method")), "method");
            echo "><div><span class=\"svg\">";
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getSVGImage", array(0 => "images/check.svg"), "method");
            echo "</span></div></a>
  ";
        }
        // line 9
        echo "  <a href=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getOpenedShopURL", array(), "method"), "html", null, true);
        echo "\" class=\"link opened\" target=\"_blank\">";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("View storefront")), "html", null, true);
        echo "</a>
  <a href=\"";
        // line 10
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getClosedShopURL", array(), "method"), "html", null, true);
        echo "\" class=\"link closed\" target=\"_blank\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Private link")), "html", null, true);
        echo "\">
    ";
        // line 11
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Storefront is closed")), "html", null, true);
        echo "
  </a>
  ";
        // line 14
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "main_center/page_container_parts/header_parts/storefront_status.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 14,  51 => 11,  45 => 10,  38 => 9,  28 => 7,  26 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Storefront status*/
/*  #}*/
/* */
/* <div {{ this.printTagAttributes(this.getContainerTagAttributes())|raw }}>*/
/*   {% if this.isTogglerVisible() %}*/
/*     <a href="{{ this.getLink() }}" {{ this.printTagAttributes(this.getTogglerTagAttributes())|raw }}><div><span class="svg">{{ this.getSVGImage('images/check.svg')|raw }}</span></div></a>*/
/*   {% endif %}*/
/*   <a href="{{ this.getOpenedShopURL() }}" class="link opened" target="_blank">{{ t('View storefront') }}</a>*/
/*   <a href="{{ this.getClosedShopURL() }}" class="link closed" target="_blank" data-toggle="tooltip" data-placement="bottom" title="{{ t('Private link') }}">*/
/*     {{ t('Storefront is closed') }}*/
/*   </a>*/
/*   {#<span class="link closed">{{ this.getCloseTitle()|raw }}</span>#}*/
/* </div>*/
/* */
