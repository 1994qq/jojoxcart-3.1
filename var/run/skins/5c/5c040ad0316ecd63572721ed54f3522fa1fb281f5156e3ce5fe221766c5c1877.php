<?php

/* /var/www/html/xcart/skins/admin/order/packing_slip/parts/head/head.twig */
class __TwigTemplate_9ac32ccdf150c51958b89f1972077bedbcd98b98bac552da90ec29caf775da01 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "<table cellspacing=\"0\" class=\"header\">
  <tr>
    ";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "packing_slip.head"))), "html", null, true);
        echo "
  </tr>
</table>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/order/packing_slip/parts/head/head.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 8,  19 => 6,);
    }
}
/* {##*/
/*  # Invoice head*/
/*  #*/
/*  # @ListChild (list="packing_slip.base", weight="10")*/
/*  #}*/
/* <table cellspacing="0" class="header">*/
/*   <tr>*/
/*     {{ widget_list('packing_slip.head') }}*/
/*   </tr>*/
/* </table>*/
/* */
