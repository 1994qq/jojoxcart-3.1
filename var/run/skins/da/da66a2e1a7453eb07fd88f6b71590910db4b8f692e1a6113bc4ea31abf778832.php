<?php

/* /var/www/html/xcart/skins/customer/shopping_cart/parts/group.title.twig */
class __TwigTemplate_418122de62510fbd5b9988ac401deeef3c9da5c02cc653c2fa4cec32dcce9afa extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<td colspan=\"7\">
  <p class=\"group-title\">";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "group", array()), "title", array()), "html", null, true);
        echo "</p>
</td>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/shopping_cart/parts/group.title.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 8,  19 => 6,);
    }
}
/* {##*/
/*  # Shopping cart item name*/
/*  #*/
/*  # @ListChild (list="cart.group", weight="10")*/
/*  #}*/
/* */
/* <td colspan="7">*/
/*   <p class="group-title">{{ this.group.title }}</p>*/
/* </td>*/
/* */
