<?php

/* product/details/parts/gallery_mobile.twig */
class __TwigTemplate_faa9a244e11000e03f0f82c4e9d1f5fdb24356db9ad5d3158be661f6647ad44a extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"product-image-gallery";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getCSSClasses", array(), "method"), "html", null, true);
        echo "\"";
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "isVisibleAsHidden", array(), "method")) {
            echo " style=\"display:none;\"";
        }
        echo ">
  <div class=\"product-image-gallery-navigation product-image-gallery-prev-mobile\" id='product-image-gallery-prev-mobile-";
        // line 6
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getProductId", array(), "method"), "html", null, true);
        echo "' style=\"display: none\"></div>
  <div class=\"slides\">

    <ul
            class=\"cycle-cloak cycle-slideshow\"
            data-cycle-fx=carousel
            data-cycle-timeout=0
            data-cycle-manual-speed=\"300\"
            data-cycle-carousel-vertical=false
            data-cycle-carousel-visible=";
        // line 15
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getMinCountForSlider", array(), "method"), "html", null, true);
        echo "
            data-cycle-next=\"#product-image-gallery-next-mobile-";
        // line 16
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getProductId", array(), "method"), "html", null, true);
        echo "\"
            data-cycle-prev=\"#product-image-gallery-prev-mobile-";
        // line 17
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getProductId", array(), "method"), "html", null, true);
        echo "\"
            data-cycle-slides=\"> li\"
            data-cycle-log=false
            data-cycle-allow-wrap=false
            data-cycle-auto-height=false
            data-cycle-auto-init=false
    >
      ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "product", array()), "getPublicImages", array(), "method"));
        foreach ($context['_seq'] as $context["i"] => $context["image"]) {
            // line 25
            echo "        <li ";
            echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "printTagAttributes", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getListItemClassAttribute", array(0 => $context["i"], 1 => $context["image"]), "method")), "method");
            echo ">
          <a href=\"";
            // line 26
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["image"], "getFrontURL", array(), "method"), "html", null, true);
            echo "\" rel=\"lightbox\"
             rev=\"width: ";
            // line 27
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["image"], "getWidth", array(), "method"), "html", null, true);
            echo ", height: ";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["image"], "getHeight", array(), "method"), "html", null, true);
            echo "\"
             title=\"";
            // line 28
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute($context["image"], "getAlt", array(), "method"), "html", null, true);
            echo "\">";
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Image", "image" => $context["image"], "alt" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getAlt", array(0 => $context["image"], 1 => $context["i"]), "method"), "imageSizeType" => "MSThumbnail"))), "html", null, true);
            echo "</a>
          ";
            // line 29
            echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget')->getCallable(), array($this->env, $context, array(0 => "\\XLite\\View\\Image", "className" => "middle", "style" => "display: none;", "image" => $context["image"], "maxWidth" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getWidgetMaxWidth", array(), "method"), "maxHeight" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getWidgetMaxHeight", array(), "method"), "alt" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getAlt", array(0 => $context["image"], 1 => $context["i"]), "method")))), "html", null, true);
            echo "
        </li>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "    </ul>
  </div>

  <div class=\"product-image-gallery-navigation product-image-gallery-next-mobile\" id='product-image-gallery-next-mobile-";
        // line 35
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getProductId", array(), "method"), "html", null, true);
        echo "' style=\"display: none\"></div>
</div>

<script>
  var lightBoxImagesDir = '";
        // line 39
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getLightBoxImagesDir", array(), "method"), "html", null, true);
        echo "';
</script>
";
    }

    public function getTemplateName()
    {
        return "product/details/parts/gallery_mobile.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 39,  99 => 35,  94 => 32,  85 => 29,  79 => 28,  73 => 27,  69 => 26,  64 => 25,  60 => 24,  50 => 17,  46 => 16,  42 => 15,  30 => 6,  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # Gallery widget*/
/*  #}*/
/* */
/* <div class="product-image-gallery{{ this.getCSSClasses() }}"{% if this.isVisibleAsHidden() %} style="display:none;"{% endif %}>*/
/*   <div class="product-image-gallery-navigation product-image-gallery-prev-mobile" id='product-image-gallery-prev-mobile-{{ this.getProductId() }}' style="display: none"></div>*/
/*   <div class="slides">*/
/* */
/*     <ul*/
/*             class="cycle-cloak cycle-slideshow"*/
/*             data-cycle-fx=carousel*/
/*             data-cycle-timeout=0*/
/*             data-cycle-manual-speed="300"*/
/*             data-cycle-carousel-vertical=false*/
/*             data-cycle-carousel-visible={{ this.getMinCountForSlider() }}*/
/*             data-cycle-next="#product-image-gallery-next-mobile-{{ this.getProductId() }}"*/
/*             data-cycle-prev="#product-image-gallery-prev-mobile-{{ this.getProductId() }}"*/
/*             data-cycle-slides="> li"*/
/*             data-cycle-log=false*/
/*             data-cycle-allow-wrap=false*/
/*             data-cycle-auto-height=false*/
/*             data-cycle-auto-init=false*/
/*     >*/
/*       {% for i, image in this.product.getPublicImages() %}*/
/*         <li {{ this.printTagAttributes(this.getListItemClassAttribute(i, image))|raw }}>*/
/*           <a href="{{ image.getFrontURL() }}" rel="lightbox"*/
/*              rev="width: {{ image.getWidth() }}, height: {{ image.getHeight() }}"*/
/*              title="{{ image.getAlt() }}">{{ widget('\\XLite\\View\\Image', image=image, alt=this.getAlt(image, i), imageSizeType='MSThumbnail') }}</a>*/
/*           {{ widget('\\XLite\\View\\Image', className='middle', style='display: none;', image=image, maxWidth=this.getWidgetMaxWidth(), maxHeight=this.getWidgetMaxHeight(), alt=this.getAlt(image, i)) }}*/
/*         </li>*/
/*       {% endfor %}*/
/*     </ul>*/
/*   </div>*/
/* */
/*   <div class="product-image-gallery-navigation product-image-gallery-next-mobile" id='product-image-gallery-next-mobile-{{ this.getProductId() }}' style="display: none"></div>*/
/* </div>*/
/* */
/* <script>*/
/*   var lightBoxImagesDir = '{{ this.getLightBoxImagesDir() }}';*/
/* </script>*/
/* */
