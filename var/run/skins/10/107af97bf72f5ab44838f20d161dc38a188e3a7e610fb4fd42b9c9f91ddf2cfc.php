<?php

/* product/details/page/body.twig */
class __TwigTemplate_b5a1955bc12d537f7a20175e5c34a237ab527c18b918fde0092f7993fed1152d extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "<div ";
        echo $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "printTagAttributes", array(0 => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getContainerAttributes", array(), "method")), "method");
        echo ">
   
  ";
        // line 6
        $this->startForm("\\XLite\\View\\Form\\Product\\AddToCart", array("product" => $this->getAttribute((isset($context["this"]) ? $context["this"] : null), "product", array()), "className" => "product-details", "validationEngine" => "1"));        // line 7
        echo "    ";
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "product.details.page"))), "html", null, true);
        echo "
  ";
        $this->endForm();        // line 9
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "product/details/page/body.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 9,  26 => 7,  25 => 6,  19 => 4,);
    }
}
/* {##*/
/*  # Product details*/
/*  #}*/
/* <div {{ this.printTagAttributes(this.getContainerAttributes())|raw }}>*/
/*    */
/*   {% form '\\XLite\\View\\Form\\Product\\AddToCart' with {product: this.product, className: 'product-details', validationEngine: '1'} %}*/
/*     {{ widget_list('product.details.page') }}*/
/*   {% endform %}*/
/* </div>*/
/* */
