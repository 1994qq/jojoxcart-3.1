<?php

/* /var/www/html/xcart/skins/admin/upgrade/step/ready_to_install/buttons/parts/left.twig */
class __TwigTemplate_c21bf182844c2386021105e9f71a06475a05196dc779d1b63f864f74462b7f32 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<div class=\"left\">
  ";
        // line 8
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('widget_list')->getCallable(), array($this->env, $context, array(0 => "sections.left", "type" => "inherited"))), "html", null, true);
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/upgrade/step/ready_to_install/buttons/parts/left.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 8,  19 => 6,);
    }
}
/* {##*/
/*  # Left actions block*/
/*  #*/
/*  # @ListChild (list="upgrade.step.ready_to_install.buttons.sections", weight="100")*/
/*  #}*/
/* */
/* <div class="left">*/
/*   {{ widget_list('sections.left', type='inherited') }}*/
/* </div>*/
/* */
