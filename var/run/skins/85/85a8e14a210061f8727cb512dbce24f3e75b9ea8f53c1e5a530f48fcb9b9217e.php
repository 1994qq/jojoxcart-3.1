<?php

/* /var/www/html/xcart/skins/customer/checkout/success/parts/panel_line.twig */
class __TwigTemplate_8c7cc16eac5c23b1e93759b3fe65bfa74cd90431caed439212d636929ef6f1a9 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<hr class=\"tiny\" />
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/checkout/success/parts/panel_line.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Line*/
/*  #*/
/*  # @ListChild (list="checkout.success.panel", weight="300")*/
/*  #}*/
/* */
/* <hr class="tiny" />*/
/* */
