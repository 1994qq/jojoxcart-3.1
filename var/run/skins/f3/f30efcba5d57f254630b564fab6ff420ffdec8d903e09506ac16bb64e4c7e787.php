<?php

/* /var/www/html/xcart/skins/customer/items_list/product/parts/common.added-mark.twig */
class __TwigTemplate_b42ca2a0f90dbf0e158c6aa3fd6eb51c1778787e2ebe7edf4dfe8c005a23cdf1 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 9
        echo "<div title=\"Added to cart\" class=\"added-to-cart\"><i class=\"fa fa-check-square\"></i></div>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/items_list/product/parts/common.added-mark.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 9,);
    }
}
/* {##*/
/*  # Added-to-cart mark*/
/*  #*/
/*  # @ListChild (list="itemsList.product.grid.customer.info", weight="998")*/
/*  # @ListChild (list="itemsList.product.list.customer.photo", weight="998")*/
/*  # @ListChild (list="itemsList.product.small_thumbnails.customer.info", weight="998")*/
/*  # @ListChild (list="itemsList.product.big_thumbnails.customer.info")*/
/*  #}*/
/* <div title="Added to cart" class="added-to-cart"><i class="fa fa-check-square"></i></div>*/
/* */
