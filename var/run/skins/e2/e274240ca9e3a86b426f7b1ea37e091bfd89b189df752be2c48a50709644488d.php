<?php

/* /var/www/html/xcart/skins/customer/header/parts/meta_viewport.twig */
class __TwigTemplate_56fc171050eec200c1700778ed3dd0a723346414d13293940e8e55f1639ed788 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/customer/header/parts/meta_viewport.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {##*/
/*  # Head list children*/
/*  #*/
/*  # @ListChild (list="head", weight="650")*/
/*  #}*/
/* */
/* <meta name="viewport" content="width=device-width, initial-scale=1.0" />*/
/* */
