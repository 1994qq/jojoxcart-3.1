<?php

/* /var/www/html/xcart/skins/admin/modules/XC/Onboarding/wizard/close_button.twig */
class __TwigTemplate_1f6c691391eb6f641db3631fb4007f64b35e61324e5f69ed20a0610462954748 extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"close-button\" @click=\"hideWizard\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"";
        // line 5
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Close wizard")), "html", null, true);
        echo "\"></div>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/modules/XC/Onboarding/wizard/close_button.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 5,  19 => 4,);
    }
}
/* {##*/
/*  # @ListChild(list="onboarding-wizard.header", weight="50")*/
/*  #}*/
/* */
/* <div class="close-button" @click="hideWizard" data-toggle="tooltip" data-placement="left" title="{{ t('Close wizard') }}"></div>*/
