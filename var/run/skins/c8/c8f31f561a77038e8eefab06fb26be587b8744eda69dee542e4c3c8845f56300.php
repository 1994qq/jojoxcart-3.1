<?php

/* /var/www/html/xcart/skins/admin/product/search/parts/by_conditions_parts/by_title.twig */
class __TwigTemplate_6fbf0db11f1140eb1e63f7bb489cb816763c39edc0877b92b78e4b706628d38a extends \XLite\Core\Templating\Twig\Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo "
<li>
  <input type=\"checkbox\" name=\"by_title\" id=\"by-title\" value=\"Y\" ";
        // line 8
        if ($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "getCondition", array(0 => "by_title"), "method")) {
            echo " checked=\"checked\" ";
        }
        echo " />
  <label for=\"by-title\">";
        // line 9
        echo XLite\Core\Templating\Twig\Extension\xcart_twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('t')->getCallable(), array("Name")), "html", null, true);
        echo "</label>
</li>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/xcart/skins/admin/product/search/parts/by_conditions_parts/by_title.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 9,  23 => 8,  19 => 6,);
    }
}
/* {##*/
/*  # ____file_title____*/
/*  #*/
/*  # @ListChild (list="product.search.conditions.by-keywords", weight="10")*/
/*  #}*/
/* */
/* <li>*/
/*   <input type="checkbox" name="by_title" id="by-title" value="Y" {% if this.getCondition('by_title') %} checked="checked" {% endif %} />*/
/*   <label for="by-title">{{ t('Name') }}</label>*/
/* </li>*/
/* */
