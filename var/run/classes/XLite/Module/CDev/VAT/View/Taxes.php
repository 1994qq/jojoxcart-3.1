<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\View;

/**
 * Taxes widget (admin)
 */
class Taxes extends \XLite\View\AView
{
    /**
     * Register CSS files
     *
     * @return array
     */
    public function getCSSFiles()
    {
        $list = parent::getCSSFiles();

        $list[] = 'modules/CDev/VAT/admin.css';

        return $list;
    }

    /**
     * Register JS files
     *
     * @return array
     */
    public function getJSFiles()
    {
        $list = parent::getJSFiles();

        $list[] = 'modules/CDev/VAT/admin.js';

        return $list;
    }

    /**
     * Return widget default template
     *
     * @return string
     */
    protected function getDefaultTemplate()
    {
        return 'modules/CDev/VAT/edit.twig';
    }

    /**
     * Return true if common tax settings should be displayed as expanded section
     *
     * @return boolean
     */
    protected function isCommonOptionsExpanded()
    {
        $value = \XLite\Core\Config::getInstance()->CDev->VAT->common_settings_expanded;

        return !is_null($value) ? $value : true;
    }

    /**
     * Return URL to search language labels to rename 'Merchant Tax Number' label
     *
     * @return string
     */
    protected function getRenameMerchantTaxNumberLabelURL()
    {
        return \XLite\View\ItemsList\Model\Translation\Labels::getSearchLabelURL('Merchant Tax Number (VAT ID, ABN etc)');
    }
}
