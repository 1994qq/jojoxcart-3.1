<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\View;

/**
 * Invoice widget
 */
abstract class Invoice extends \XLite\Module\CDev\XPaymentsConnector\View\Invoice implements \XLite\Base\IDecorator
{
    /**
     * Register CSS files
     *
     * @return array
     */
    public function getCSSFiles()
    {
        $list = parent::getCSSFiles();
        $list[] = 'modules/CDev/VAT/order/invoice/style.css';

        return $list;
    }

    /**
     * Register CSS files
     *
     * @return array
     */
    public function getCommonFiles()
    {
        $list = parent::getCommonFiles();
        $list[static::RESOURCE_CSS][] = 'modules/CDev/VAT/order/invoice/style.css';

        return $list;
    }

    /**
     * Get tax
     *
     * @return \XLite\Module\CDev\VAT\Model\Tax
     */
    public function getVatTax()
    {
        return \XLite\Core\Database::getRepo('XLite\Module\CDev\VAT\Model\Tax')->getTax();
    }

    /**
     * Get tax name
     *
     * @return string
     */
    public function getVatName()
    {
        return $this->getVatTax()->getName();
    }

    /**
     * Return true if VAT was applied to the order
     *
     * @return boolean
     */
    public function isUseVatInvoiceMode()
    {
        return $this->getOrder()->isUseVatInvoiceMode();
    }

    /**
     * Return additional CSS classes for invoice box
     *
     * @return string
     */
    protected function getInvoiceBoxAdditionalCSS()
    {
        $result = parent::getInvoiceBoxAdditionalCSS();

        if ($this->isUseVatInvoiceMode()) {
            $result .= ' vat-is-applied-mode';
        }

        return $result;
    }

    // {{{ Mail

    /**
     * Returns address field template
     *
     * @param string $type  Address type
     * @param array  $field Address field returned by \XLite\View\AView#getAddressSectionData
     *
     * @return string
     */
    protected function getAddressFieldVatNumberTemplate($type, $field)
    {
        return \XLite\Model\Address::BILLING == $type
            ? 'modules/CDev/VAT/order/invoice/parts/address_field/vat_number.twig'
            : null;
    }

    /**
     * List of hidden title address fields
     *
     * @return array
     */
    protected function getAddressFieldTitleHiddenList()
    {
        return array_merge(
            parent::getAddressFieldTitleHiddenList(),
            array('vat_number')
        );
    }

    // }}}
}
