<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\View\Model\Address;

/**
 * Profile model widget
 */
abstract class Address extends \XLite\View\Model\Address\AddressAbstract implements \XLite\Base\IDecorator
{
    /**
     * getAddressSchema
     *
     * @return array
     */
    public function getAddressSchema()
    {
        $result = parent::getAddressSchema();
        $addressId = $this->getAddressId();

        foreach ($result as $key => $data) {
            if ($addressId . '_' . 'vat_number' == $key) {
                $result[$key][static::SCHEMA_DEPENDENCY] = array(
                    static::DEPENDENCY_SHOW => array(
                        $addressId . '_' . 'country_code'
                            => \XLite\Module\CDev\VAT\Core\VATNumberChecker::getAllowedCountries(),
                    ),
                );
            }
        }

        return $result;
    }
}
