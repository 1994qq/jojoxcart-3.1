<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\View;

use \XLite\Module\CDev\VAT\Main;

/**
 * Price widget 
 */
 class Price extends \XLite\View\PriceAbstract implements \XLite\Base\IDecorator
{
    /**
     * Register CSS files
     *
     * @return array
     */
    public function getCSSFiles()
    {
        $list = parent::getCSSFiles();
        if ($this->isVATApplicable()) {
            $list[] = 'modules/CDev/VAT/price.css';
        }

        return $list;
    }
    /**
     * Determine if we need to display 'incl.VAT' note
     *
     * @return boolean
     */
    protected function isVATApplicable()
    {
        $result = false;
        $optionValue = \XLite\Core\Config::getInstance()->CDev->VAT->display_inc_vat_label;

        if (
            ('P' == $optionValue && in_array(\XLite\Core\Request::getInstance()->target, $this->getProductTargets()))
            || 'Y' == $optionValue
        ) {
            $product = $this->getProduct();

            // Exclude taxes with "0" value
            $taxes = array_filter(
                $product->getIncludedTaxList()
            );

            $result = !empty($taxes);
        }

        return $result;
    }

    /**
     * Get targets of product pages
     * 
     * @return array
     */
    protected function getProductTargets()
    {
        return array('product', 'quick_look');
    }

    /**
     * Determine if we need to display 'incl.VAT' note
     *
     * @return boolean
     */
    protected function isDisplayedPriceIncludingVAT()
    {
        return Main::isDisplayPricesIncludingVAT();
    }

    /**
     * Determine if we need to display 'incl.VAT' note
     *
     * @return boolean
     */
    protected function getVATName()
    {
        $tax = \XLite\Core\Database::getRepo('XLite\Module\CDev\VAT\Model\Tax')->getTax();

        return $tax->name;
    }
}
