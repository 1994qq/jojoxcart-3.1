<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\View;

/**
 * Checkout
 */
abstract class Checkout extends \XLite\Module\CDev\XPaymentsConnector\View\Checkout implements \XLite\Base\IDecorator
{

    /**
     * Get preloaded labels
     *
     * @return array
     */
    public function getPreloadedLanguageLabels()
    {
        $list = array(
            'VAT number is valid',
            'VAT number format is valid',
            'VAT number is invalid',
            'Wrong VAT number format for selected country.',
        );

        $data = parent::getPreloadedLanguageLabels();

        foreach ($list as $name) {
            $data[$name] = static::t($name);
        }

        return $data;
    }
}
