<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Controller\Admin;

/**
 * Checkout
 */
abstract class AddressBook extends \XLite\Controller\Admin\AddressBookAbstract implements \XLite\Base\IDecorator
{
    /**
     * Define the actions with no secure token
     *
     * @return array
     */
    public static function defineFreeFormIdActions()
    {
        return array_merge(parent::defineFreeFormIdActions(), array('check_vat_number'));
    }

    /**
     * Check VAT number action
     *
     * @return void
     */
    protected function doActionCheckVatNumber()
    {
        $vatNumber = \XLite\Core\Request::getInstance()->vatNumber;
        $countryCode = \XLite\Core\Request::getInstance()->countryCode;

        $result = null;

        if ($vatNumber && $countryCode) {
            $result = \XLite\Module\CDev\VAT\Core\VATNumberChecker::getInstance()
                ->checkVATNumber($countryCode, $vatNumber);
        }

        $this->setSuppressOutput(true);        
        $this->silent = true;
        $this->displayJSON($result);
    }

    /**
     * Prepare address data
     *
     * @param array  $data Address data
     * @param string $type Address type OPTIONAL
     *
     * @return array
     */
    protected function prepareAddressData(array $data, $type = 'shipping')
    {
        $data = parent::prepareAddressData($data, $type);

        if (array_key_exists('vat_number', $data) && is_null($data['vat_number'])) {
            $data['vat_number'] = '';
        }

        $allowedCountries = \XLite\Module\CDev\VAT\Core\VATNumberChecker::getAllowedCountries();

        if (array_key_exists('country_code', $data) && !in_array($data['country_code'], $allowedCountries)) {
            $data['vat_number'] = '';
        }

        return $data;
    }
}
