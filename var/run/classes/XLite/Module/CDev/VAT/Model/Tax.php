<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Model;

/**
 * Tax
 *
 * @Entity
 * @Table  (name="vat_taxes")
 */
class Tax extends \XLite\Model\Base\I18n
{
    /**
     * Tax unique ID
     *
     * @var integer
     *
     * @Id
     * @GeneratedValue (strategy="AUTO")
     * @Column         (type="integer", options={ "unsigned": true })
     */
    protected $id;

    /**
     * Eenabled
     *
     * @var boolean
     *
     * @Column (type="boolean")
     */
    protected $enabled = false;

    /**
     * Tax rates (relation)
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @OneToMany (targetEntity="XLite\Module\CDev\VAT\Model\Tax\Rate", mappedBy="tax", cascade={"all"})
     * @OrderBy ({"position" = "ASC"})
     */
    protected $rates;

    /**
     * VAT base membership
     *
     * @var \XLite\Model\Membership
     *
     * @ManyToOne  (targetEntity="XLite\Model\Membership", cascade={"detach", "merge", "persist"})
     * @JoinColumn (name="vat_membership_id", referencedColumnName="membership_id", onDelete="CASCADE")
     */
    protected $vatMembership;

    /**
     * VAT base Zone
     *
     * @var \XLite\Model\Zone
     *
     * @ManyToOne  (targetEntity="XLite\Model\Zone", cascade={"detach", "merge", "persist"})
     * @JoinColumn (name="vat_zone_id", referencedColumnName="zone_id", onDelete="CASCADE")
     */
    protected $vatZone;

    /**
     * Constructor
     *
     * @param array $data Entity properties OPTIONAL
     *
     * @return void
     */
    public function __construct(array $data = array())
    {
        $this->rates = new \Doctrine\Common\Collections\ArrayCollection();

        parent::__construct($data);
    }

    /**
     * Get applicable rates by zones and membership
     *
     * @param array                   $zones      Zone id list
     * @param \XLite\Model\Membership $membership Membership OPTIONAL
     *
     * @return array
     */
    public function getApplicableRates(array $zones, \XLite\Model\Membership $membership = null) {
        $rates = array();

        foreach ($this->getRates() as $rate) {
            $id = $rate->getTaxClass()
                ? ($rate->getNoTaxClass() ? -1 : $rate->getTaxClass()->getId())
                : 0;
            if ($rate->isApplied($zones, $membership, null, true) && !isset($rates[$id])) {
                $rates[$id] = $rate;
                if (!$id) {
                    break;
                }
            }
        }

        return $rates;
    }

    /**
     * Get filtered rates by zones, membership and tax class
     *
     * @param array                   $zones      Zone id list
     * @param \XLite\Model\Membership $membership Membership OPTIONAL
     * @param \XLite\Model\TaxClass   $taxClass   Tax class OPTIONAL
     *
     * @return array
     */
    public function getFilteredRates(
        array $zones,
        \XLite\Model\Membership $membership = null,
        \XLite\Model\TaxClass $taxClass = null
    ) {
        $rates = array();

        $ratesList = array();

        foreach ($this->getRates() as $rate) {
            foreach ($zones as $i => $zone) {
                if ($rate->isApplied(array($zone), $membership, $taxClass)) {
                    $ratesList[] = array(
                        'rate'      => $rate,
                        'zoneWeight' => $i,
                        'ratePos'    => $rate->getPosition(),
                    );
                    break;
                }
            }
        }

        usort($ratesList, array($this, 'sortRates'));

        foreach ($ratesList as $rate) {
            $rates[] = $rate['rate'];
        }

        return $rates;
    }

    /**
     * Get filtered rate by zones, membership and tax class
     *
     * @param array                   $zones      Zone id list
     * @param \XLite\Model\Membership $membership Membership OPTIONAL
     * @param \XLite\Model\TaxClass   $taxClass   Tax class OPTIONAL
     *
     * @return \XLite\Module\CDev\VAT\Model\Tax\Rate
     */
    public function getFilteredRate(
        array $zones,
        \XLite\Model\Membership $membership = null,
        \XLite\Model\TaxClass $taxClass = null
    ) {
        $rates = $this->getFilteredRates($zones, $membership, $taxClass);

        return array_shift($rates);
    }

   /**
     * Sort rates
     *
     * @param array $a Rate A
     * @param array $b Rate B
     *
     * @return boolean
     */
    protected function sortRates($a, $b)
    {
        if ($a['zoneWeight'] > $b['zoneWeight']) {
            $result = 1;

        } elseif ($a['zoneWeight'] < $b['zoneWeight']) {
            $result = -1;

        } else {
            $result = $a['ratePos'] < $b['ratePos'] ? -1 : (int) $a['ratePos'] > $b['ratePos'];
        }

        return $result;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Tax
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Add rates
     *
     * @param \XLite\Module\CDev\VAT\Model\Tax\Rate $rates
     * @return Tax
     */
    public function addRates(\XLite\Module\CDev\VAT\Model\Tax\Rate $rates)
    {
        $this->rates[] = $rates;
        return $this;
    }

    /**
     * Get rates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRates()
    {
        return $this->rates;
    }

    /**
     * Set vatMembership
     *
     * @param \XLite\Model\Membership $vatMembership
     * @return Tax
     */
    public function setVatMembership(\XLite\Model\Membership $vatMembership = null)
    {
        $this->vatMembership = $vatMembership;
        return $this;
    }

    /**
     * Get vatMembership
     *
     * @return \XLite\Model\Membership 
     */
    public function getVatMembership()
    {
        return $this->vatMembership;
    }

    /**
     * Set vatZone
     *
     * @param \XLite\Model\Zone $vatZone
     * @return Tax
     */
    public function setVatZone(\XLite\Model\Zone $vatZone = null)
    {
        $this->vatZone = $vatZone;
        return $this;
    }

    /**
     * Get vatZone
     *
     * @return \XLite\Model\Zone 
     */
    public function getVatZone()
    {
        return $this->vatZone;
    }
}
