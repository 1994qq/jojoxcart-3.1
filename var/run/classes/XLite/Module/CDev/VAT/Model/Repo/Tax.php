<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Model\Repo;

/**
 * @Api\Operation\Read(modelClass="XLite\Module\CDev\VAT\Model\Tax", summary="Retrieve tax type by id")
 * @Api\Operation\ReadAll(modelClass="XLite\Module\CDev\VAT\Model\Tax", summary="Retrieve tax types")
 * @Api\Operation\Update(modelClass="XLite\Module\CDev\VAT\Model\Tax", summary="Update tax type by id")
 *
 * @SWG\Tag(
 *   name="CDev\VAT\Tax",
 *   x={"display-name": "Tax", "group": "CDev\VAT"},
 *   description="Tax model keeps track of tax types, such as VAT type, Sales Tax type etc. It can have from one to many tax rate records.",
 *   @SWG\ExternalDocumentation(
 *     description="Find out more about setting up taxes",
 *     url="https://kb.x-cart.com/en/taxes/"
 *   )
 * )
 */
class Tax extends \XLite\Model\Repo\Base\I18n
{
    /**
     * Get tax
     *
     * @return \XLite\Module\CDev\VAT\Model\Tax
     */
    public function getTax()
    {
        $tax = $this->createQueryBuilder()
            ->setMaxResults(1)
            ->getSingleResult();

        if (!$tax) {
            $tax = $this->createTax();
        }

        return $tax;
    }

    /**
     * Find active taxes
     *
     * @return array
     */
    public function findActive()
    {
        return $this->defineFindActiveQuery()->getResult();
    }

    /**
     * Define query for findActive() method
     *
     * @return \XLite\Model\QueryBuilder\AQueryBuilder
     */
    protected function defineFindActiveQuery()
    {
        return $this->createQueryBuilder()
            ->linkInner('t.rates')
            ->andWhere('t.enabled = :true')
            ->setParameter('true', true);
    }

    /**
     * Create tax
     *
     * @return \XLite\Module\CDev\VAT\Model\Tax
     */
    protected function createTax()
    {
        $tax = new \XLite\Module\CDev\VAT\Model\Tax;
        $tax->setName('VAT');
        $tax->setEnabled(true);

        $zone = \XLite\Core\Database::getRepo('XLite\Model\Zone')->findOneBy(array('zone_name' => 'Europe'));
        if ($zone) {
            $tax->setVatZone($zone);
        }

        \XLite\Core\Database::getEM()->persist($tax);
        \XLite\Core\Database::getEM()->flush();

        return $tax;
    }
}
