<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Model\Repo;

/**
 * Product repository
 */
 class Product extends \XLite\Module\CDev\XMLSitemap\Model\Repo\Product implements \XLite\Base\IDecorator
{
    /**
     * Define calculated price definition DQL
     *
     * @param \XLite\Model\QueryBuilder\AQueryBuilder $queryBuilder Query builder
     * @param string                                  $alias        Main alias
     *
     * @return string
     */
    protected function defineCalculatedPriceDQL(\XLite\Model\QueryBuilder\AQueryBuilder $queryBuilder, $alias)
    {
        $dql = parent::defineCalculatedPriceDQL($queryBuilder, $alias);

        if (!\XLite::isAdminZone()) {
            $dql = \XLite\Module\CDev\VAT\Logic\Product\Tax::getInstance()
                ->getSearchPriceCondition($queryBuilder, $dql);
            $dql = '(' . $dql . ')';
        }

        return $dql;
    }
}
