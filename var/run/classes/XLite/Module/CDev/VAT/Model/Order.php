<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Model;


/**
 * Order class extension
 */
 class Order extends \XLite\Module\CDev\XPaymentsConnector\Model\Order implements \XLite\Base\IDecorator
{
    /**
     * Run-time cache flag
     *
     * @var boolean
     */
    protected $canApplyVAT;

    /**
     * Return true if VAT can be applied to order
     *
     * @return boolean
     */
    public function canApplyVAT()
    {
        if (!isset($this->canApplyVAT)) {
            $modifier = $this->getModifier(
                \XLite\Model\Base\Surcharge::TYPE_TAX,
                \XLite\Module\CDev\VAT\Logic\Order\Modifier\Tax::MODIFIER_CODE
            );

            $this->canApplyVAT = $modifier && $modifier->canApply();
        }

        return $this->canApplyVAT;
    }

    /**
     * Get items included surcharges totals
     *
     * @param boolean $forCartItems Flag: true - return values for cart items only, false - for cart totals and items
     *
     * @return array
     */
    public function getItemsIncludeSurchargesTotals($forCartItems = false)
    {
        $list = parent::getItemsIncludeSurchargesTotals($forCartItems);

        if ($forCartItems) {
            // Remove VAT surcharge calculated for cart items
            foreach ($list as $k => $v) {
                $code = $v['surcharge']->getCode();
                if (preg_match('/' . preg_quote(\XLite\Module\CDev\VAT\Logic\Order\Modifier\Tax::MODIFIER_CODE) . '/', $code)) {
                    unset($list[$k]);
                }
            }
        }

        return $list;
    }

    /**
     * Return true if VAT was applied to the order
     *
     * @return boolean
     */
    public function isUseVatInvoiceMode()
    {
        return (bool) $this->getDetail('isAppliedVAT');
    }

    /**
     * Return true if option display_prices_including_vat was enabled at the moment of order placed
     *
     * @return boolean
     */
    public function isOrderCalculatedIncludingVAT()
    {
        return $this->getDetailValue('display_prices_including_vat');
    }

    /**
     * Called when an order successfully placed by a client
     *
     * @return void
     */
    public function processSucceed()
    {
        parent::processSucceed();

        $event = $this->getPendingVatVerificationEvent();
        if ($event && $event['id'] == $this->getOrderId()) {

            \XLite\Core\OrderHistory::getInstance()->registerVatVerificationEvent(
                $event['id'],
                $event['data']
            );

            $this->forgetVatVerificationEvent();
        }
    }

    /**
     * Returns latest VAT verification event
     * 
     * @return array
     */
    protected function getPendingVatVerificationEvent()
    {
        return \XLite\Core\Session::getInstance()->vat_verification_event;
    }

    /**
     * Writes latest VAT verification event to session
     * 
     * @param  array $data Event data
     */
    public function rememberVatVerificationEvent($data)
    {
        \XLite\Core\Session::getInstance()->vat_verification_event = array(
            'id' => $this->getOrderId(),
            'data' => $data
        );
    }

    /**
     * Removes VAT verification event from session
     */
    protected function forgetVatVerificationEvent()
    {
        unset(\XLite\Core\Session::getInstance()->vat_verification_event);
    }
}
