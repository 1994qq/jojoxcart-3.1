<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Logic;

use \XLite\Module\CDev\VAT\Main;

/**
 * Display price modificator: add VAT to displayPrice
 */
class ExcludedVAT extends \XLite\Logic\ALogic
{
    /**
     * Check modificator - apply or not
     *
     * @param \XLite\Model\AEntity $model     Model
     * @param string               $property  Model's property
     * @param array                $behaviors Behaviors
     * @param string               $purpose   Purpose
     * 
     * @return boolean
     */
    static public function isApply(\XLite\Model\AEntity $model, $property, array $behaviors, $purpose)
    {
        $order = $model instanceOf \XLite\Model\OrderItem
            ? $model->getOrder()
            : null;

        return $model->getTaxable()
            && in_array('taxable', $behaviors)
            && Main::isDisplayPricesIncludingVAT($order);
    }

    /**
     * Modify money 
     * 
     * @param float                $value     Value
     * @param \XLite\Model\AEntity $model     Model
     * @param string               $property  Model's property
     * @param array                $behaviors Behaviors
     * @param string               $purpose   Purpose
     *  
     * @return float
     */
    static public function modifyMoney($value, \XLite\Model\AEntity $model, $property, array $behaviors, $purpose)
    {
        $obj = ($model instanceOf \XLite\Model\Product ? $model : $model->getProduct());

        return \XLite\Module\CDev\VAT\Logic\Product\Tax::getInstance()->getVATValue($obj, $value) + $value;
    }
}

