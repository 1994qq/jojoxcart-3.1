<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Logic\Order\Modifier;

use \XLite\Module\CDev\VAT\Main;

/**
 * Tax  business logic
 */
class Tax extends \XLite\Logic\Order\Modifier\ATax
{
    const MODIFIER_CODE = 'CDEV.VAT';

    /**
     * Taxes (cache)
     *
     * @var array
     */
    protected $taxes;

    /**
     * Zones (cache)
     *
     * @var array
     */
    protected $zones;

    /**
     * Has tax exemption (cache)
     *
     * @var boolean
     */
    protected $taxExemption;

    /**
     * Modifier unique code
     *
     * @var string
     */
    protected $code = self::MODIFIER_CODE;

    /**
     * Surcharge identification pattern
     *
     * @var string
     */
    protected $identificationPattern = '/^CDEV\.VAT\.(\d+)(?:\.[A-Z]+)?$/Ss';

    /**
     * Sorting weight
     *
     * @var integer
     */
    protected $sortingWeight = 300;

    /**
     * Get surcharge code
     *
     * @return string
     */
    public static function getSurchargeCode()
    {
        $result = null;

        $taxes = \XLite\Core\Database::getRepo('XLite\Module\CDev\VAT\Model\Tax')->findActive();

        if ($taxes) {
            // Get the first tax, ignore the rest
            // TODO: Rework this after multi-tax will be removed
            foreach ($taxes as $tax) {
                $result =  \XLite\Module\CDev\VAT\Logic\Order\Modifier\Tax::MODIFIER_CODE . '.' . $tax->getId();
                break;
            }
        }

        return $result;
    }

    /**
     * Check - can apply this modifier or not
     *
     * @return boolean
     */
    public function canApply()
    {
        return parent::canApply() && $this->getTaxes();
    }

    // {{{ Calculation

    /**
     * Calculate
     *
     * @return array
     */
    public function calculate()
    {
        $result = array();

        $isDisplayPricesIncludingVAT = Main::isDisplayPricesIncludingVAT($this->order);

        $zones = $this->getZonesList();
        $membership = $this->getMembership();

        // Shipping cost VAT
        $modifier = $this->order->getModifier(\XLite\Model\Base\Surcharge::TYPE_SHIPPING, 'SHIPPING');
        $taxes = array();
        if ($modifier && $modifier->getSelectedRate() && $modifier->getSelectedRate()->getMethod()) {
            $taxes = $this->getShippingTaxRates($modifier->getSelectedRate(), $zones, $membership);
        }

        $taxExemption = $this->hasTaxExemption();

        foreach ($this->getTaxes() as $tax) {

            $sum = 0;
            $rates = array();
            $taxId = $tax->getId();

            foreach ($this->getTaxableItems() as $item) {
                $product = $item->getProduct();
                $rate = $tax->getFilteredRate($zones, $membership, $product->getTaxClass());
                if ($rate) {
                    if (!isset($rates[$rate->getId()])) {
                        $rates[$rate->getId()] = array(
                            'rate' => $rate,
                            'base' => 0,
                        );
                    }

                    $base = $item->getDiscountedSubtotal();

                    if ($isDisplayPricesIncludingVAT && !$taxExemption) {
                        if ($item->getVATBase() != $item->getDiscountedSubtotal()) {
                            $base = $base - $rate->calculateValueExcludingTax($base);
                        }
                    }

                    $itemSum = $taxExemption ? 0 : $rate->calculateValueIncludingTax($base);

                    if ($itemSum && $isDisplayPricesIncludingVAT) {
                        $this->addOrderItemSurcharge(
                            $item,
                            $this->code . '.' . $taxId,
                            $itemSum,
                            $isDisplayPricesIncludingVAT
                        );
                    }

                    $item->setVatRate($taxExemption ? ('0' . $rate->getType()) : $rate);

                    $rates[$rate->getId()]['base'] += $base;
                }
            }

            $shippingVAT = 0;
            $shippingVATRate = 0;

            foreach ($rates as $rate) {

                if (isset($taxes[$taxId]) && $taxes[$taxId]['rate']->getId() == $rate['rate']->getId()) {
                    $shippingVATRate = $rate['rate']->getValue();
                    $shippingBase = $taxes[$taxId]['base'];
                    $rate['base'] += $shippingBase;

                    $shippingVAT = $taxExemption ? 0 : $rate['rate']->calculateValueIncludingTax($shippingBase);

                    unset($taxes[$taxId]);
                }

                $sum += $taxExemption ? 0 : $rate['rate']->calculateValueIncludingTax($rate['base']);
            }

            // Add shipping cost VAT
            if (isset($taxes[$taxId]) && !$taxExemption) {
                $shippingVATRate = $taxes[$taxId]['rate']->getValue();
                $shippingBase = $this->order->getCurrency()->roundValue($taxes[$taxId]['base']);
                $shippingVAT += $taxes[$taxId]['rate']->calculateValueIncludingTax($shippingBase);
                $sum += $shippingVAT;
            }

            $result[] = $this->addOrderSurcharge(
                $this->code . '.' . $taxId,
                $sum,
                $isDisplayPricesIncludingVAT
            );
        }

        // Save some data for invoice
        $this->order->setDetail('shippingVATRate', $result ? $shippingVATRate : null);
        $this->order->setDetail('shippingVAT', $result ? $shippingVAT : null);
        $this->order->setDetail(
            'display_prices_including_vat',
            $result ? $isDisplayPricesIncludingVAT : null
        );

        $this->order->setDetail('isAppliedVAT', $result ? true : null);

        return $result;
    }

    /**
     * Get shipping tax rates
     *
     * @param \XLite\Model\Shipping\Rate $rate       Shipping rate
     * @param array                      $zones      Zones list
     * @param \XLite\Model\Membership    $membership Membership OPTIONAL
     *
     * @return array
     */
    protected function getShippingTaxRates(\XLite\Model\Shipping\Rate $rate, array $zones, \XLite\Model\Membership $membership = null)
    {
        $method = $rate->getMethod();

        $isDisplayPricesIncludingVAT = Main::isDisplayPricesIncludingVAT($this->order);

        $taxes = array();
        $price = $this->order->getSurchargeSumByType(\XLite\Model\Base\Surcharge::TYPE_SHIPPING);
        // $price = $rate->getTaxableBasis();
        foreach ($this->getTaxes() as $tax) {
            // $includedZones = $tax->getVATZone() ? array($tax->getVATZone()->getZoneId()) : array();
            $included = $tax->getFilteredRate($this->getZonesList(), $tax->getVATMembership(), $method->getTaxClass());
            $r = $tax->getFilteredRate($zones, $membership, $method->getTaxClass());

            if ($included && $isDisplayPricesIncludingVAT) {
                $price -= $included->calculateValueExcludingTax($price);
            }
            if ($r) {
                $taxes[$tax->getId()] = array(
                    'rate' => $r,
                    'base' => $price,
                );
            }
        }

        return $taxes;
    }

    /**
     * Get taxes
     *
     * @param boolean $force Force renew taxes list flag OPTIONAL
     *
     * @return array
     */
    protected function getTaxes($force = false)
    {
        if (!isset($this->taxes) || $force) {
            $this->taxes = $this->defineTaxes();
        }

        return $this->taxes;
    }

    /**
     * Define taxes
     *
     * @return array
     */
    protected function defineTaxes()
    {
        return \XLite\Core\Database::getRepo('XLite\Module\CDev\VAT\Model\Tax')->findActive();
    }

    /**
     * Get taxable order items
     *
     * @return \XLite\Model\OrderItem[]
     */
    protected function getTaxableItems()
    {
        $list = array();

        foreach ($this->getOrder()->getItems() as $item) {
            $product = $item->getProduct();
            if ($product && $product->getTaxable()) {
                $list[] = $item;
            }
        }

        return $list;
    }

    /**
     * Get zones list
     *
     * @param boolean $force Force renew zones list flag OPTIONAL
     *
     * @return array
     */
    protected function getZonesList($force = false)
    {
        if (!isset($this->zones) || $force) {
            $address = $this->getAddress();

            $this->zones = $address ? \XLite\Core\Database::getRepo('XLite\Model\Zone')->findApplicableZones($address) : array();

            foreach ($this->zones as $i => $zone) {
                $this->zones[$i] = $zone->getZoneId();
            }

        }

        return $this->zones;
    }

    /**
     * Get membership
     *
     * @return \XLite\Model\Membership
     */
    protected function getMembership()
    {
        return $this->getOrder()->getProfile()
            ? $this->getOrder()->getProfile()->getMembership()
            : null;
    }

    /**
     * Get address for zone calculator
     *
     * @return array
     */
    protected function getAddress()
    {
        $address = null;
        $addressObj = $this->getOrderAddress();
        if ($addressObj) {
            // Profile is exists
            $address = $addressObj->toArray();
        }

        if (!isset($address)) {
            $address = $this->getDefaultAddress();
        }

        return $address;
    }

    /**
     * Get order-based address
     *
     * @return \XLite\Model\Address
     */
    protected function getOrderAddress()
    {
        $profile = $this->getOrder()->getProfile();
        $result = null;

        if ($profile) {
            $result = 'shipping' == \XLite\Core\Config::getInstance()->CDev->VAT->addressType
                ? $profile->getShippingAddress()
                : $profile->getBillingAddress();

            if (!$result) {
                $result = $profile->getShippingAddress();
            }
        }

        return $result;
    }

    // }}}

    // {{{ Surcharge operations

    /**
     * Get surcharge name
     *
     * @param \XLite\Model\Base\Surcharge $surcharge Surcharge
     *
     * @return \XLite\DataSet\Transport\Order\Surcharge
     */
    public function getSurchargeInfo(\XLite\Model\Base\Surcharge $surcharge)
    {
        $info = new \XLite\DataSet\Transport\Order\Surcharge();

        if (preg_match($this->identificationPattern, $surcharge->getCode(), $match)) {
            $id = (int) $match[1];
            $code = (isset($match[2]) && $match[2]) ? $match[2] : null;
            $tax = \XLite\Core\Database::getRepo('XLite\Module\CDev\VAT\Model\Tax')->find($id);
            $info->name = $tax
                ? $tax->getName() . $this->getNameSuffix($surcharge)
                : \XLite\Core\Translation::lbl('VAT');

        } else {
            $info->name = \XLite\Core\Translation::lbl('VAT');
        }

        $info->notAvailableReason = \XLite\Core\Translation::lbl('Billing address is not defined');

        return $info;
    }

    /**
     * Get tax name suffix by the following condition:
     * - if all order items have the same VAT rate, then suffix should be ' X%'
     * - if order items have different VAT rates, then suffix should be empty
     *
     * @param \XLite\Model\Base\Surcharge $surcharge Order surcharge
     *
     * @return string
     */
    protected function getNameSuffix($surcharge)
    {
        $result = null;

        $order = $surcharge ? $surcharge->getOwner() : null;

        if ($order && $order instanceof \XLite\Model\Order) {
            $vatRates = array();

            foreach ($order->getItems() as $item) {
                if ($item->getVatRateValue()
                    && \XLite\Module\CDev\VAT\Model\Tax\Rate::TYPE_PERCENT == $item->getVatRateType()
                ) {
                    $vatRates[$item->getVatRateString()] = true;
                }
            }

            if (1 === count($vatRates)) {
                $result = ' ' . key($vatRates);
            }
        }

        return $result;
    }

    // }}}

    // {{{ Tax exemption

    /**
     * Has tax exemption
     *
     * @return boolean
     */
    protected function hasTaxExemption()
    {
        return \XLite\Module\CDev\VAT\Logic\ATax::checkVATExemption($this->getOrder()->getProfile());
    }

    // }}}
}
