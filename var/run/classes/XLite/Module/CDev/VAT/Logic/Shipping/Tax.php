<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Logic\Shipping;

/**
 * Tax business logic for shipping cost
 */
class Tax extends \XLite\Module\CDev\VAT\Logic\ATax
{

    // {{{ Calculation

    /**
     * Calculate rate cost
     * 
     * @param \XLite\Model\Shipping\Rate $rate  Rate
     * @param float                      $price Price
     *  
     * @return float
     */
    public function calculateRateCost(\XLite\Model\Shipping\Rate $rate, $price)
    {
        return $price;
    }

    /**
     * Calculate shipping net price
     * 
     * @param \XLite\Model\Shipping\Rate $rate  Rate
     * @param float                      $price Price
     *  
     * @return float
     */
    public function deductTaxFromPrice(\XLite\Model\Shipping\Rate $rate, $price)
    {
        $class = $rate->getMethod() && $rate->getMethod()->getTaxClass() 
            ? $rate->getMethod()->getTaxClass() : null;

        foreach ($this->getTaxes() as $tax) {
            // $includedZones = $tax->getVATZone() ? array($tax->getVATZone()->getZoneId()) : array();
            $included = $tax->getFilteredRate($this->getZonesList(), $tax->getVATMembership(), $class);

            if ($included) {
                $price -= $included->calculateValueExcludingTax($price);
            }
        }

        return $price;
    }

    // }}}
}
