<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Core;

/**
 * Check vat number at http://apilayer.net (Vatlayer)
 */
class OfflineChecker extends \XLite\Module\CDev\VAT\Core\AVATNumberChecker
{
    /**
     * Check if VAT number is valid
     *
     * @param string $countryCode Country code
     * @param string $vatNumber   VAT number
     *
     * @return mixed
     */
    public function isValid($countryCode, $vatNumber)
    {
        $result = false;

        if ($countryCode && $vatNumber) {
            $regex = $this->getRegexByCountryCode($countryCode);

            $fullVat = $countryCode . strtoupper($vatNumber);
            $result = preg_match($regex, $fullVat);
        }

        return $result ? VATNumberChecker::STATUS_FORMAT_VALID : VATNumberChecker::STATUS_INVALID;
    }

    /**
     * Returns regex to test VAT Numbers using country code
     * 
     * @param  string $countryCode ISO 3166 alpha-2 code
     * @return string
     */
    protected function getRegexByCountryCode($countryCode)
    {
        switch ($countryCode) {
            case 'AT':
                return '/^(AT)U(\d{8})$/';

            case 'BE':
                return '/^(BE)(0?\d{9})$/';

            case 'BG':
                return '/^(BG)(\d{9,10})$/';

            case 'CH':
                return '/^(CH)E(\d{9})(MWST)?$/';

            case 'CY':
                return '/^(CY)([0-59]\d{7}[A-Z])$/';

            case 'CZ':
                return '/^(CZ)(\d{8,10})(\d{3})?$/';

            case 'DE':
                return '/^(DE)([1-9]\d{8})/';

            case 'DK':
                return '/^(DK)(\d{8})/';

            case 'EE':
                return '/^(EE)(10\d{7})$/';

            case 'GR':
                return '/^(EL|GR)(\d{9})$/';

            case 'ES':
                return '/^(ES)(?:([A-Z]\d{8})|([A-HN-SW]\d{7}[A-J])|([0-9YZ]\d{7}[A-Z])|([KLMX]\d{7}[A-Z]))$/';

            case 'FI':
                return '/^(FI)(\d{8})$/';

            case 'FR':
                return '/^(FR)(?:(\d{11})|([A-HJ-NP-Z]\d{10})|(\d[A-HJ-NP-Z]\d{9})|([A-HJ-NP-Z]{2}\d{9}))$/';

            case 'GB':
                return '/^(GB)?(?:(\d{9})|(\d{12})|(GD\d{3})|(HA\d{3}))$/';

            case 'HR':
                return '/^(HR)(\d{11})$/';

            case 'HU':
                return '/^(HU)(\d{8})$/';

            case 'IE':
                return '/^(IE)(?:(\d{7}[A-W])|([7-9][A-Z\*\+)]\d{5}[A-W])|(\d{7}[A-W][AH]))$/';

            case 'IT':
                return '/^(IT)(\d{11})$/';

            case 'LT':
                return '/^(LT)(\d{9}|\d{12})$/';

            case 'LU':
                return '/^(LU)(\d{8})$/';

            case 'LV':
                return '/^(LV)(\d{11})$/';

            case 'MT':
                return '/^(MT)([1-9]\d{7})$/';

            case 'NL':
                return '/^(NL)(\d{9})B\d{2}$/';

            case 'PL':
                return '/^(PL)(\d{10})$/';

            case 'PT':
                return '/^(PT)(\d{9})$/';

            case 'RO':
                return '/^(RO)([1-9]\d{1,9})$/';

            case 'SE':
                return '/^(SE)(\d{10}01)$/';

            case 'SI':
                return '/^(SI)([1-9]\d{7})$/';

            case 'SK':
                return '/^(SK)([1-9]\d[2346-9]\d{7})$/';
                
            case 'RS':
                return '/^(RS)(\d{9})$/';

            case 'RU':
                return '/^(RU)(\d{10}|\d{12})$/';

            default:
                return '/^(EU)(\d{9})$/';
        }
    }
}