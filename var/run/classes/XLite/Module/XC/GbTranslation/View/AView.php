<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\XC\GbTranslation\View;

/**
 * Abstract widget
 */
abstract class AView extends \XLite\Module\XC\ThemeTweaker\View\AView implements \XLite\Base\IDecorator
{
    /**
     * Return theme common files
     *
     * @return array
     */
    protected function getThemeFiles($adminZone = null)
    {
        $list = parent::getThemeFiles($adminZone);
        if (\XLite\Module\XC\GbTranslation\Main::isActiveLanguage()) {
            $list[static::RESOURCE_CSS][] = 'modules/XC/GbTranslation/css/layout.css';
        }

        return $list;
    }
}
