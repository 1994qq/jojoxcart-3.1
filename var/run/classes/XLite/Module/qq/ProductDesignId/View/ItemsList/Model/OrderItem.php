<?php
// vim: set ts=4 sw=4 sts=4 et:

namespace XLite\Module\qq\ProductDesignId\View\ItemsList\Model;

abstract class OrderItem extends \XLite\View\ItemsList\Model\OrderItemAbstract implements \XLite\Base\IDecorator
{
    use \XLite\View\Base\ViewListsFallbackTrait;

    /**
     * Define columns structure
     *
     * @return array
     */
    protected function defineColumns()
    {
        return array(
            'name' => array(
                static::COLUMN_NAME         => static::t('Item'),
                static::COLUMN_CREATE_CLASS => 'XLite\View\FormField\Inline\Select\Model\Product\OrderItem',
                static::COLUMN_TEMPLATE     => 'items_list/model/table/order_item/cell.name.twig',
                static::COLUMN_PARAMS       => array(
                    'required' => true,
                    \XLite\View\FormField\Select\Model\OrderItemSelector::PARAM_ORDER_ID => $this->getOrder()->getOrderId(),
                ),
                static::COLUMN_MAIN         => true,
                static::COLUMN_ORDERBY      => 100,
            ),
            'designId' => array(
                static::COLUMN_NAME     => static::t('Design Id'),
                static::COLUMN_ORDERBY  => 200,
            ),
            'price' => array(
                static::COLUMN_NAME     => static::t('Price'),
                static::COLUMN_CLASS    => 'XLite\View\FormField\Inline\Input\Text\Price\OrderItemPrice',
                static::COLUMN_PARAMS   => array(
                    \XLite\View\FormField\Input\Text\Base\Numeric::PARAM_MIN              => 0,
                    \XLite\View\FormField\Input\Text\Base\Numeric::PARAM_MOUSE_WHEEL_CTRL => false,
                ),
                static::COLUMN_ORDERBY  => 200,
            ),
            'amount' => array(
                static::COLUMN_NAME    => static::t('Qty'),
                static::COLUMN_CLASS   => 'XLite\View\FormField\Inline\Input\Text\Integer\OrderItemAmount',
                static::COLUMN_PARAMS  => array('required' => true),
                static::COLUMN_ORDERBY => 300,
            ),
            'total' => array(
                static::COLUMN_NAME     => static::t('Total'),
                static::COLUMN_TEMPLATE => 'items_list/model/table/order_item/cell.total.twig',
                static::COLUMN_CREATE_TEMPLATE => 'items_list/model/table/order_item/cell.total.twig',
                static::COLUMN_ORDERBY  => 400,
            ),
        );
    }
}