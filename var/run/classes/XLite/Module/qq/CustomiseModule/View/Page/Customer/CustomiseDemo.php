<?php

namespace XLite\Module\qq\CustomiseModule\View\Page\Customer;

/**
* @ListChild (list="center")
*/

class CustomiseDemo extends \XLite\View\AView
{
   public static function getAllowedTargets()
   {
       return array_merge(parent::getAllowedTargets(), array('example_popup_demo'));
   }

   protected function getDefaultTemplate()
   {
       return 'customer/product/add_button/body.twig';
   }
}   
