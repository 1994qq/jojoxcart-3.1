<?php
namespace XLite\Module\qq\CustomiseModule;

abstract class Main extends \XLite\Module\AModule
{
   /**
    * Author name
    *
    * @return string
    */
   public static function getAuthorName()
   {
       return 'Joan';
   }

   /**
    * Module name
    *
    * @return string
    */
   public static function getModuleName()
   {
       return 'CustomiseButton';
   }

   /**
    * Get module major version
    *
    * @return string
    */
   public static function getMajorVersion()
   {
       return '5.3';
   }

   /**
    * Module version
    *
    * @return string
    */
   public static function getMinorVersion()
   {
       return 0;
   }

   /**
    * Module description
    *
    * @return string
    */
   public static function getDescription()
   {
       return 'for customise button';
   }
}
