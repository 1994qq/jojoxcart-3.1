<?php

namespace XLite\Module\qq\EditDesignButton\View\Page\Button;

/**
* Demo CustomiseButton widget
*/
class DemoEditDesignButton extends \XLite\View\Button\AButton
{
 

   /**
    * Return URL parameters to use in AJAX popup
    *
    * @return array
    */
   /**
    * Return CSS classes
    *
    * @return string
    */
   protected function getClass()
   {
       return parent::getClass() . ' confirm-with-password';
   }

   protected function getDefaultTemplate()
   {
       return 'modules/qq/EditDesignButton/EditDesignButton.twig';
   }

}