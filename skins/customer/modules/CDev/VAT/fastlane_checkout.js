/* vim: set ts=2 sw=2 sts=2 et: */

/**
 * Shipping address controller
 *
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

define(
    'vat/checkout_fastlane/blocks/cart_items',
    ['checkout_fastlane/blocks/cart_items', 'checkout_fastlane/sections'],
    function (CartItems, Sections) {

      var CartItemsNew = CartItems.extend({

        events: {
          global_updatecart: function (data) {
            var triggerKeys = ['shippingAddressFields', 'billingAddressFields', 'sameAddress'];
            var needsUpdate = _.some(triggerKeys, function (key) {
              return _.has(data, key);
            });

            if (needsUpdate) {
              this.$reload();
            }
          },

        },
      });

      Vue.registerComponent(Sections, CartItemsNew);

      return CartItemsNew;
    }
);
