<?php
// vim: set ts=4 sw=4 sts=4 et:

namespace XLite\Module\qq\CustomiseModule\Model\OrderItem;

/**
 * Something customer can put into his cart
 */
abstract class OrderItem extends \XLite\Model\OrderItem implements \XLite\Base\IDecorator
{
    /**
     * @Column (type="string")
     */
    protected $attribute_value = array();



    public function getAttributeValues()
    {
        return $this->attribute_values;   
    }

        
    public function setAttributeValues($value)
    {
        $this->attribute_values = $value;
        return $this;
    }

}