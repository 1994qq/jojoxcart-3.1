<?php

namespace XLite\Module\qq\CustomiseModule\Controller\Customer;


class CustomiseDemo extends \XLite\Controller\Customer\ACustomer implements \XLite\Base\IDecorator
{
    public function getAttributeValues()
   { 
       return \XLite\Core\Request::getInstance()->attribute_values;
   }
 protected function prepareOrderItem(\XLite\Model\Product $product = null, $amount = null)
   {
       $item = null;

       if ($product) {
           $item = new \XLite\Model\OrderItem();
           $item->setOrder($this->getCart());
           $item->setAttributeValues(
       
        $product->prepareAttributeValues(\XLite\Core\Request::getInstance()->attribute_values)
           );
           $item->setProduct($product);

           $item->setDesignId($this->getDesignId());

           // We make amount correction if there is no such product with additional specifications
           // which are provided in order item container
           $newAmount = $this->correctAmountToAdd($item, $amount);

           if (0 < $newAmount) {
               $item->setAmount($newAmount);
           } else {
               $item->setOrder(null);
               $item = null;
           }
       }

       return $item;
   }


} 
