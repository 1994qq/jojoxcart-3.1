<?php

namespace XLite\Module\qq\CustomiseModule\View\Page\Button;

  
/**
* Demo CustomiseButton widget
*/
class CustomiseButton extends \XLite\View\Button\AButton
{
   
  
   /**
    * Return URL parameters to use in AJAX popup
    *
    * @return array
    */
   /**
    * Return CSS classes
    *
    * @return string
    */
   protected function getClass()
   {
       return parent::getClass() . ' confirm-with-password';
   }

   protected function getDefaultTemplate()
   {
       return 'modules/qq/CustomiseModule/CustomiseButton.twig';
   }
  


}
