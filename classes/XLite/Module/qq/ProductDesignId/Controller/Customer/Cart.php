<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\qq\ProductDesignId\Controller\Customer;

use XLite\Core\Database\Migration\UnsupportedDatabaseOperationDuringMaintenanceException;

/**
 * \XLite\Controller\Customer\Cart
 */
class Cart extends \XLite\Controller\Customer\Cart implements \XLite\Base\IDecorator
{
    
    
            
    protected $designId;

    

    public function getDesignId()
    {
        // $return = '';
            
        // if (isset($_GET['design_id'])) {
        //     $return = $_GET['design_id'];
        // }
            
        // return $return;

        if (empty(\XLite\Core\Request::getInstance()->design_id)) {
            return '';
        }

        return \XLite\Core\Request::getInstance()->design_id;

    }

 
    

    /**
     * Prepare order item class for adding to cart.
     * This method takes \XLite\Model\Product class and amount and creates \XLite\Model\OrderItem.
     * This order item container will be added to cart in $this->addItem() method.
     *
     * @param \XLite\Model\Product $product Product class to add to cart OPTIOANL
     * @param integer              $amount  Amount of product to add to cart OPTIONAL
     *
     * @return \XLite\Model\OrderItem
     */
    protected function prepareOrderItem(\XLite\Model\Product $product = null, $amount = null)
    {
        $item = null;

        if ($product) {
            $item = new \XLite\Model\OrderItem();
            $item->setOrder($this->getCart());
            $item->setAttributeValues(
                $product->prepareAttributeValues(\XLite\Core\Request::getInstance()->attribute_values)
            );
            $item->setProduct($product);

            $item->setDesignId($this->getDesignId());

            // We make amount correction if there is no such product with additional specifications
            // which are provided in order item container
            $newAmount = $this->correctAmountToAdd($item, $amount);

            if (0 < $newAmount) {
                $item->setAmount($newAmount);
            } else {
                $item->setOrder(null);
                $item = null;
            }
        }

        return $item;
    }

    /**
     * Add order item to cart.
     * Additional correction of item amount is made before adding.
     *
     * @param \XLite\Model\OrderItem $item Order item
     *
     * @return boolean
     */
    protected function addItem($item)
    {
        if ($item) {
            /** @var \XLite\Model\Cart $cart */
            $cart = $this->getCart();
            $cart->setIgnoreLongCalculations();

            if (!$cart->isPersistent()) {
                \XLite\Core\Database::getEM()->persist($cart);
                \XLite\Core\Database::getEM()->flush();

                \XLite\Core\Session::getInstance()->order_id = $cart->getOrderId();
            }
        }

        return $item && $this->getCart()->addItem($item);
    }

}