<?php
// vim: set ts=4 sw=4 sts=4 et:

namespace XLite\Module\qq\ProductDesignId\Model\OrderItem;

/**
 * Something customer can put into his cart
 */
abstract class OrderItem extends \XLite\Model\OrderItem implements \XLite\Base\IDecorator
{
    /**
     * @Column (type="string")
     */
    protected $designId;



    public function getDesignId()
    {
        return $this->designId;   
    }

        
    public function setDesignId($value)
    {
        $this->designId = $value;
        return $this;
    }

    public function getKey()
    {
        $result = self::PRODUCT_TYPE . '.' . ($this->getObject() ? $this->getObject()->getId() : null);
        foreach ($this->getAttributeValues() as $attributeValue) {
            $result .= '||'
                . $attributeValue->getActualName()
                . '::'
                . $attributeValue->getActualValue();
        }
        

        $result .= '||designId::' . $this->getDesignId();
   
        return $result;
    }


    // protected function saveItemState(\XLite\Model\Base\IOrderItem $item)
    // {
    //     parent::saveItemState($item);

    //     // $this->designId = $item->getDesignId();
    // }

    // protected function resetItemState()
    // {
    //     parent::resetItemState();
        
    //     $this->designId = '';
    // }

    // protected function getDeletedProduct()
    // {
    //     $dumpProduct = parent::getDeletedProduct();
    //     // $dumpProduct->setDesignId($this->getDesignId());

    //     return $dumpProduct;
    // }

}