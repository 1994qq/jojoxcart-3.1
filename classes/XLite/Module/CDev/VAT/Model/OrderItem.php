<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Model;

use \XLite\Module\CDev\VAT\Main;

/**
 * OrderItem class extension
 */
class OrderItem extends \XLite\Model\OrderItem implements \XLite\Base\IDecorator
{
    /**
     * Item VAT rate: <rate value><rate type> (e.g. 10%, 10)
     *
     * @var string
     *
     * @Column (type="string", length=32, nullable=true)
     */
    protected $vatRate;

    /**
     * Order item VAT rate value
     *
     * @var float
     */
    protected $vatRateValue;

    /**
     * Order item VAT rate type (percent or absolute)
     *
     * @var string
     */
    protected $vatRateType;

    /**
     * Order item VAT base
     *
     * @var float|null
     */
    protected $vatBase = null;

    /**
     * Run-time cache of isDisplayPricesIncludingVAT value
     *
     * @var boolean
     */
    protected $isDisplayPricesIncludingVAT;

    /**
     * Get order item tax base
     *
     * @return float
     */
    public function getVATBase()
    {
        return isset($this->vatBase) ? $this->vatBase : $this->getDiscountedSubtotal();
    }

    /**
     * Set order item tax base
     *
     * @param float $value
     *
     * @return void
     */
    public function setVATBase($value)
    {
        return $this->vatBase = $value;
    }

    /**
     * Get value of flag isDisplayPricesIncludingVAT
     *
     * @return boolean
     */
    protected function isDisplayPricesIncludingVAT()
    {
        if (!isset($this->isDisplayPricesIncludingVAT)) {
            $this->isDisplayPricesIncludingVAT = Main::isDisplayPricesIncludingVAT($this->getOrder());
        }

        return $this->isDisplayPricesIncludingVAT;
    }

    /**
     * Define net price
     *
     * @return float
     */
    protected function defineNetPrice()
    {
        return $this->isDisplayPricesIncludingVAT()
            ? $this->getDisplayPrice()
            : parent::defineNetPrice();
    }

    /**
     * Get item net price
     *
     * @return float
     */
    public function getItemNetPrice()
    {
        return $this->isOrderOpen() && $this->isDisplayPricesIncludingVAT()
            ? $this->getDisplayPrice()
            : parent::getItemNetPrice();
    }

    /**
     * Return true if VAT can be applied to the order
     *
     * @return boolean
     */
    public function canApplyVAT()
    {
        return $this->getOrder()
            ? $this->getOrder()->canApplyVAT()
            : false;
    }

    /**
     * Initial calculate order item
     *
     * @return void
     */
    public function calculate()
    {
        parent::calculate();

        if ($this->canApplyVAT() && $this->isDisplayPricesIncludingVAT()) {

            $price = $this->isOrderOpen()
                ? $this->getNetPrice()
                : $this->getNetPriceExcludingVAT();

            $discountedSubtotal = $price * $this->getAmount();
            $this->setDiscountedSubtotal($discountedSubtotal);
            $this->setVATBase($discountedSubtotal);
        }
    }

    /**
     * Get order item net price (excluding VAT)
     * This method is used value saved in the database 'price' field
     *
     * @return float
     */
    public function getNetPriceExcludingVAT()
    {
        return \XLite\Logic\Price::getInstance()->apply($this, 'getPrice', array('taxable'), 'net');
    }

    /**
     * Get order item's VAT rate
     *
     * @return string
     */
    public function getVatRate()
    {
        return $this->vatRate;
    }

    /**
     * Set order item VAT rate
     *
     * @param string|\XLite\Module\CDev\VAT\Model\Tax\Rate $rate Rate object or its string representation
     *
     * @return void
     */
    public function setVatRate($rate)
    {
        $value = null;

        if ($rate instanceOf \XLite\Module\CDev\VAT\Model\Tax\Rate) {
            $value = $rate->getValue() . $rate->getType();

        } elseif (is_string($rate) && $this->initializeVatRate($rate)) {
            $value = $this->vatRateValue . $this->vatRateType;
        }

        $this->vatRate = $value;
    }

    /**
     * Get order item's VAT rate value
     *
     * @return float
     */
    public function getVatRateValue()
    {
        if (!isset($this->vatRateValue)) {
            $this->initializeVatRate();
        }

        return false !== $this->vatRateValue ? $this->vatRateValue : null;
    }

    /**
     * Get order item's VAT rate type
     *
     * @return string
     */
    public function getVatRateType()
    {
        if (!isset($this->vatRateType)) {
            $this->initializeVatRate();
        }

        return false !== $this->vatRateType ? $this->vatRateType : null;
    }

    /**
     * Get VAT rate representation as a string
     *
     * @return string
     */
    public function getVatRateString()
    {
        $result = '';

        if (null !== $this->getVatRateValue()) {

            if (\XLite\Module\CDev\VAT\Model\Tax\Rate::TYPE_PERCENT == $this->getVatRateType()) {
                $type = '%';
                $value = \XLite\Logic\Math::getInstance()->round($this->getVatRateValue(), 3);
                $value = 0 < $value - intval($value)
                    ? $value
                    : intval($value);
                $result = $value . $type;

            } else {
                $result = $this->getOrder()->getCurrency()->formatValue($this->getVatRateValue());
            }

        }

        return $result;
    }

    /**
     * Initialize vatRateType and vatRateValue properties.
     * Returns true if initialization is successful
     *
     * @param string $newRate New rate
     *
     * @return boolean
     */
    protected function initializeVatRate($newRate = null)
    {
        $result = false;

        $rate = is_null($newRate) ? $this->getVatRate() : $newRate;

        $rateTypes = array(
            \XLite\Module\CDev\VAT\Model\Tax\Rate::TYPE_ABSOLUTE,
            \XLite\Module\CDev\VAT\Model\Tax\Rate::TYPE_PERCENT,
        );

        if ($rate && preg_match('/^([\d.]+)(' . implode('|', $rateTypes) . ')$/', $rate, $match)) {
            $this->vatRateType = $match[2];
            $this->vatRateValue = (float)$match[1];
            $result = true;

        } else {
            $this->vatRateType = $this->vatRateValue = false;
        }

        return $result;
    }
}
