<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Model\Shipping;

/**
 * Shipping rate model
 */
class Rate extends \XLite\Model\Shipping\Rate implements \XLite\Base\IDecorator
{
    /**
     * Get total rate
     *
     * @return float
     */
    public function getTotalRate()
    {
        $total = parent::getTotalRate();

        if ($this->getMethod()) {
            $total = \XLite\Module\CDev\VAT\Logic\Shipping\Tax::getInstance()->calculateRateCost($this, $total);
        }

        return $total;
    }
}
