<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Model\Repo\Tax;

/**
 * @Api\Operation\Create(modelClass="XLite\Module\CDev\VAT\Model\Tax\Rate", summary="Add tax rate")
 * @Api\Operation\Read(modelClass="XLite\Module\CDev\VAT\Model\Tax\Rate", summary="Retrieve tax rate by id")
 * @Api\Operation\ReadAll(modelClass="XLite\Module\CDev\VAT\Model\Tax\Rate", summary="Retrieve tax rates by conditions")
 * @Api\Operation\Update(modelClass="XLite\Module\CDev\VAT\Model\Tax\Rate", summary="Update tax rate by id")
 * @Api\Operation\Delete(modelClass="XLite\Module\CDev\VAT\Model\Tax\Rate", summary="Delete tax rate by id")
 *
 * @SWG\Tag(
 *   name="CDev\VAT\Tax\Rate",
 *   x={"display-name": "Tax\Rate", "group": "CDev\VAT"},
 *   description="Tax\Rate represents single tax rate record associated with the VAT tax type.",
 *   @SWG\ExternalDocumentation(
 *     description="Find out more about setting up taxes",
 *     url="https://kb.x-cart.com/en/taxes/"
 *   )
 * )
 */
class Rate extends \XLite\Model\Repo\ARepo
{
    /**
     * Default 'order by' field name
     *
     * @var string
     */
    protected $defaultOrderBy = 'position';
}
