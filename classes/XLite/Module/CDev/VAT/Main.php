<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT;

use XLite\Model\AddressField;

/**
 * Main
 */
abstract class Main extends \XLite\Module\AModule
{
    /**
     * Author name
     *
     * @return string
     */
    public static function getAuthorName()
    {
        return 'X-Cart team';
    }

    /**
     * Get module major version
     *
     * @return string
     */
    public static function getMajorVersion()
    {
        return '5.3';
    }

    /**
     * Module version
     *
     * @return string
     */
    public static function getMinorVersion()
    {
        return '4';
    }

    /**
     * Get module build number (4th number in the version)
     *
     * @return string
     */
    public static function getBuildVersion()
    {
        return '3';
    }

    /**
     * Get minor core version which is required for the module activation
     *
     * @return string
     */
    public static function getMinorRequiredCoreVersion()
    {
        return '4';
    }

    /**
     * Module name
     *
     * @return string
     */
    public static function getModuleName()
    {
        return 'Value Added Tax / Goods and Services Tax';
    }

    /**
     * Module description
     *
     * @return string
     */
    public static function getDescription()
    {
        return 'Set value added tax rates for customers (VAT, GST - taxes specific for UK, EU, Australia and Canada). Use different tax rates based on customer\'s membership level and address. Tax exemption based on customer\'s VAT ID is supported.';
    }

    /**
     * Determines if we need to show settings form link
     *
     * @return boolean
     */
    public static function showSettingsForm()
    {
        return false;
    }

    /**
     * Return true if prices should be displayed including VAT
     *
     * @param \XLite\Model\Order $order Order object OPTIONAL
     *
     * @return boolean
     */
    public static function isDisplayPricesIncludingVAT($order = null)
    {
        return $order && $order instanceOf \XLite\Model\Order && $order->isUseVatInvoiceMode()
            ? (bool) $order->isOrderCalculatedIncludingVAT()
            : (bool) \XLite\Core\Config::getInstance()->CDev->VAT->display_prices_including_vat;
    }

    /**
     * @return AddressField|null
     */
    protected static function getVatAddressField()
    {
        return \XLite\Core\Database::getRepo('XLite\Model\AddressField')
            ->findOneByServiceName('vat_number');
    }

    /**
     * @throws \Exception
     */
    public static function callDisableEvent()
    {
        parent::callDisableEvent();

        if ($field = static::getVatAddressField()) {
            /* @var AddressField $field */
            \XLite\Core\Database::getRepo('XLite\Model\Config')->createOption([
                'category' => 'CDev\VAT',
                'name'     => 'vat_adr_field_auto_disabled',
                'value'    => $field->getEnabled(),
            ]);

            $field->setEnabled(false);
        }
    }

    /**
     * @throws \Exception
     */
    public static function runBuildCacheHandler()
    {
        parent::runBuildCacheHandler();

        if (
            \XLite\Core\Config::getInstance()->CDev->VAT->vat_adr_field_auto_disabled
            && $field = static::getVatAddressField()
        ) {
            \XLite\Core\Database::getRepo('XLite\Model\Config')->createOption([
                'category' => 'CDev\VAT',
                'name'     => 'vat_adr_field_auto_disabled',
                'value'    => false,
            ]);

            $field->setEnabled(true);
        }
    }

    public static function callUninstallEvent()
    {
        parent::callUninstallEvent();

        if ($field = static::getVatAddressField()) {
            \XLite\Core\Database::getEM()->remove($field);
        }
    }
}
