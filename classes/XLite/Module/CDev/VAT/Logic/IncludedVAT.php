<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Logic;

/**
 * Net price modificator: exclude VAT from price (if price is stored including VAT)
 */
class IncludedVAT extends \XLite\Logic\ALogic
{
    /**
     * Check modificator - apply or not
     *
     * @param \XLite\Model\AEntity $model     Model
     * @param string               $property  Model's property
     * @param array                $behaviors Behaviors
     * @param string               $purpose   Purpose
     * 
     * @return boolean
     */
    static public function isApply(\XLite\Model\AEntity $model, $property, array $behaviors, $purpose)
    {
        return $model->getTaxable()
            && in_array('taxable', $behaviors);
    }

    /**
     * Modify money 
     * 
     * @param float                $value     Value
     * @param \XLite\Model\AEntity $model     Model
     * @param string               $property  Model's property
     * @param array                $behaviors Behaviors
     * @param string               $purpose   Purpose
     *  
     * @return float
     */
    static public function modifyMoney($value, \XLite\Model\AEntity $model, $property, array $behaviors, $purpose)
    {
        $obj = ($model instanceOf \XLite\Model\Product ? $model : $model->getProduct());

        return \XLite\Module\CDev\VAT\Logic\Product\Tax::getInstance()->deductTaxFromPrice($obj, $value);
    }
}

