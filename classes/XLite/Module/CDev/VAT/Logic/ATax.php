<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Logic;

/**
 * Common tax business logic (base class for product and shipping tax logic)
 */
abstract class ATax extends \XLite\Logic\ALogic
{
    /**
     * Taxes (cache)
     *
     * @var array
     */
    protected $taxes;

    /**
     * Zones (cache)
     *
     * @var array
     */
    protected $zones;

    /**
     * Get taxes
     *
     * @param boolean $force Force renew taxes list flag OPTIONAL
     *
     * @return array
     */
    protected function getTaxes($force = false)
    {
        if (!isset($this->taxes) || $force) {
            $this->taxes = $this->defineTaxes();
        }

        return $this->taxes;
    }

    /**
     * Define taxes
     *
     * @return array
     */
    protected function defineTaxes()
    {
        return \XLite\Core\Database::getRepo('XLite\Module\CDev\VAT\Model\Tax')->findActive();
    }

    /**
     * Get zones list
     *
     * @param boolean $force Force renew zones list flag OPTIONAL
     *
     * @return array
     */
    protected function getZonesList($force = false)
    {
        if (!isset($this->zones) || $force) {
            $address = $this->getAddress();

            $this->zones = $address ? \XLite\Core\Database::getRepo('XLite\Model\Zone')->findApplicableZones($address) : array();

            foreach ($this->zones as $i => $zone) {
                $this->zones[$i] = $zone->getZoneId();
            }

        }

        return $this->zones;
    }

    /**
     * Get membership
     *
     * @return \XLite\Model\Membership
     */
    protected function getMembership()
    {
        return $this->getProfile()->getMembership();
    }

    /**
     * Get profile
     *
     * @return \XLite\Model\Profile
     */
    protected function getProfile()
    {
        $controller = \XLite::getController();

        $profile = $controller instanceof \XLite\Controller\Customer\ACustomer
            ? $controller->getCart(true)->getProfile()
            : \XLite\Core\Auth::getInstance()->getProfile();

        return $profile ?: $this->getDefaultProfile();
    }

    /**
     * Get default profile if user is not authorized
     *
     * @return \XLite\Model\Profile
     */
    protected function getDefaultProfile()
    {
        return new \XLite\Model\Profile;
    }

    /**
     * Get address for zone calculator
     *
     * @return array
     */
    protected function getAddress()
    {
        $address = null;
        $addressObj = 'shipping' == \XLite\Core\Config::getInstance()->CDev->VAT->addressType
            ? $this->getProfile()->getShippingAddress()
            : $this->getProfile()->getBillingAddress();

        if (!$addressObj) {
            $addressObj = $this->getProfile()->getShippingAddress();
        }

        if ($addressObj) {
            // Profile is exists
            $address = $addressObj->toArray();
        }

        if (!isset($address)) {
            $address = \XLite\Model\Shipping::getDefaultAddress();
        }

        return $address;
    }

    // }}}

    // {{{ Tax exemption

    /**
     * Return true if cart/order has VAT exemption
     *
     * @param \XLite\Model\Profile $profile Customer profile
     *
     * @return boolean
     */
    public static function checkVATExemption($profile)
    {
        $address = $profile
            ? (
                'shipping' == \XLite\Core\Config::getInstance()->CDev->VAT->addressType
                    ? $profile->getShippingAddress()
                    : $profile->getBillingAddress()
            )
            : null;

        if ($address && $address->getVatNumber() && static::checkVatCountry($address)) {
            $vat_status = \XLite\Module\CDev\VAT\Core\VATNumberChecker::getInstance()
                ->checkVATNumber($address->getCountryCode(), $address->getVatNumber());
            return $vat_status === \XLite\Module\CDev\VAT\Core\VATNumberChecker::STATUS_VALID
                || $vat_status === \XLite\Module\CDev\VAT\Core\VATNumberChecker::STATUS_FORMAT_VALID;
        } else {
            return false;
        }
    }

    /**
     * Return true if specified address country does not match shop country
     *
     * @param \XLite\Model\Address $address Customer address
     *
     * @return boolean
     */
    protected static function checkVatCountry($address)
    {
        return $address->getCountry()->getCode() != static::getShopCountryCode();
    }

    /**
     * Get shop country
     *
     * @return string
     */
    protected static function getShopCountryCode()
    {
        return \XLite\Core\Config::getInstance()->Company->location_country;
    }

    /**
     * Has tax exemption
     *
     * @return boolean
     */
    protected function hasTaxExemption()
    {
        $profile = $this->getProfile();

        return static::checkVATExemption($profile);
    }

    // }}}
}
