<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Logic\Product;

use \XLite\Module\CDev\VAT\Main;

/**
 * Product tax business logic
 */
class Tax extends \XLite\Module\CDev\VAT\Logic\ATax
{
    // {{{ Product search

    /**
     * Get search price condition
     *
     * @param \XLite\Model\QueryBuilder\AQueryBuilder $queryBuilder Query builder
     * @param string                                  $priceField   Price field name (ex. 'p.price')
     *
     * @return string
     */
    public function getSearchPriceCondition($queryBuilder, $priceField)
    {
        $zones = $this->getZonesList();
        $membership = $this->getMembership();

        $isDisplayPricesIncludingVAT = Main::isDisplayPricesIncludingVAT($this->order);

        $cnd = $priceField;

        foreach ($this->getTaxes() as $tax) {
            $rates = array();
            $includedZones = $tax->getVATZone() ? array($tax->getVATZone()->getZoneId()) : array();
            $taxRates = $tax->getApplicableRates($includedZones, $tax->getVATMembership());
            if ($isDisplayPricesIncludingVAT) {
                $rates = $tax->getApplicableRates($zones, $membership);
                if (
                    $rates
                    && $taxRates
                    && count($rates) == count($taxRates) 
                ) {
                    $identicalCount = 0;
                    foreach ($taxRates as $classId => $rate) {
                        if (
                            isset($rates[$classId])
                            && $rates[$classId]->getId() == $rate->getId()
                        ) {
                            $identicalCount++;
                        }
                    }

                    if (count($rates) == $identicalCount) {
                        $rates = $taxRates = array();
                    }
                }
            }

            if ($taxRates) {
                $cnd .= ' - (';
                foreach ($taxRates as $classId => $rate) {
                    if (-1 == $classId) {
                        $cnd .= 'IF(p.taxClass IS NULL, ';

                    } elseif ($classId) {
                        $alias = 'tax2Classe' . $classId;
                        $queryBuilder->leftJoin('p.taxClass', $alias, 'WITH', $alias . '.id =' . $classId);
                        $cnd .= 'IF(' . $alias . '.id > 0, ';

                    }
                    $cnd .= $rate->getExcludeTaxFormula($priceField);
                    if ($classId) {
                        $cnd .= ', ';
                    }
                }
                if (isset($taxRates[0])) {
                    unset($taxRates[$classId]);

                } else {
                    $cnd .= '0';
                }
                $cnd .= str_repeat(')', count($taxRates) + 1);
            }

            if ($rates) {
                $priceField = $cnd ? '(' . $cnd . ')' : $priceField;
                $cnd = '';
                foreach ($rates as $classId => $rate) {
                    if (-1 == $classId) {
                        $cnd .= 'IF(p.taxClass IS NULL, ';

                    } elseif ($classId) {
                        $alias = 'taxClasse' . $classId;
                        $queryBuilder->leftJoin('p.taxClass', $alias, 'WITH', $alias . '.id =' . $classId);
                        $cnd .= 'IF(' . $alias . '.id > 0, ';

                    }
                    $cnd .= $rate->getIncludeTaxFormula($priceField);
                    if ($classId) {
                        $cnd .= ', ';
                    }
                }
                if (isset($rates[0])) {
                    unset($rates[$classId]);

                } else {
                    $cnd .= '0';
                }
                $cnd .= str_repeat(')', count($rates));
            }
        }

        return $cnd;
    }

    // }}}

    // {{{ Calculation

    /**
     * Calculate product-based included taxes
     * 
     * @param \XLite\Model\Product $product Product
     * @param float                $price   Price OPTIONAL
     *  
     * @return array
     */
    public function calculateProductTaxes(\XLite\Model\Product $product, $price)
    {
        $zones = $this->getZonesList(true);
        $membership = $this->getMembership();

        $taxes = array();

        if (!$this->hasTaxExemption()) {
            foreach ($this->getTaxes() as $tax) {

                $rate = $tax->getFilteredRate($zones, $membership, $product->getTaxClass());

                if ($rate) {
                    $taxes[$tax->getName()] = $rate->calculateProductPriceIncludingTax($product, $price);
                }
            }
        }

        return $taxes;
    }

    /**
     * Calculate VAT value for specified product and price
     * 
     * @param \XLite\Model\Product $product Product model object
     * @param float                $price   Price
     *  
     * @return float
     */
    public function getVATValue(\XLite\Model\Product $product, $price)
    {
        $taxes = $this->calculateProductTaxes($product, $price);

        $taxTotal = 0;

        if (!empty($taxes)) {
            foreach ($taxes as $tax) {
                $taxTotal += $tax;
            }
        }

        return $this->hasTaxExemption()
            ? 0
            : $taxTotal;
    }

    /**
     * Calculate product net price
     * 
     * @param \XLite\Model\Product $product Product
     * @param float                $price   Price
     *  
     * @return float
     */
    public function deductTaxFromPrice(\XLite\Model\Product $product, $price)
    {
        foreach ($this->getTaxes() as $tax) {
            $includedZones = $tax->getVATZone() ? array($tax->getVATZone()->getZoneId()) : array();
            $included = $tax->getFilteredRate($includedZones, $tax->getVATMembership(), $product->getTaxClass());

            if ($included) {
                $price -= $included->calculateProductPriceExcludingTax($product, $price);
            }
        }

        return $price;
    }

    // }}}
}
