<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Core;

/**
 * OrderHistory
 */
class OrderHistory extends \XLite\Core\OrderHistory implements \XLite\Base\IDecorator
{
    const CODE_VAT_NONVERIFIED = 'VAT NON VERIFIED';
    const TXT_VAT_NONVERIFIED  = 'Couldn\'t verify Customer VAT IN due to service unavailability';
    const TXT_VAT_NONVERIFIED_COMMENT  = ' [VAT IN: {{vatnumber}}, country code: {{countrycode}}]';

    /**
     * Register the result of vat verification
     *
     * @param integer $orderId Order id
     * @param array   $changes Verification event data
     *
     * @return void
     */
    public function registerVatVerificationEvent($orderId, array $eventData)
    {
        if ($eventData && $eventData['result'] == 'format_valid') {
            $this->registerEvent(
                $orderId,
                static::CODE_VAT_NONVERIFIED,
                static::TXT_VAT_NONVERIFIED . $this->getVatVerificationComment($eventData),
                array()
            );
        }
    }

    /**
     * Get content for CODE_VAT_NONVERIFIED event comments
     *
     * @param integer $orderId Order id
     * @param array $eventData Verification event data
     *
     * @return string
     */
    protected function getVatVerificationComment($eventData)
    {
        return static::t(static::TXT_VAT_NONVERIFIED_COMMENT, array('vatnumber' => $eventData['vatnumber'], 'countrycode' => $eventData['countrycode']));
    }
}
