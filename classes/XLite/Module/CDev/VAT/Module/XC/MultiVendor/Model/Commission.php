<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Module\XC\MultiVendor\Model;

use XLite\Model\Order\Surcharge;


/**
 * Commission
 *
 * @Decorator\Depend("XC\MultiVendor")
 */
class Commission extends \XLite\Module\XC\MultiVendor\Model\Commission implements \XLite\Base\IDecorator
{
    protected function getOrderCommission()
    {
        return parent::getOrderCommission() + $this->getVATCommission();
    }

    /**
     * @return float|int
     */
    protected function getVATCommission()
    {
        $base = 0;

        foreach ($this->getOrder()->getSurcharges() as $surcharge) {
            /* @var Surcharge $surcharge */
            if (
                $surcharge->getModifier()
                && $surcharge->getModifier()->getModifier() instanceof \XLite\Module\CDev\VAT\Logic\Order\Modifier\Tax
            ) {
                $base += $surcharge->getValue();

                if (
                    \XLite\Module\CDev\VAT\Main::isDisplayPricesIncludingVAT($this->getOrder())
                    && ($detail = $this->getOrder()->getDetail('shippingVAT'))
                ) {
                    /* @var \XLite\Model\OrderDetail $detail */
                    $base -= $detail->getValue();
                }
            }
        }

        $modifierBase = $this->getOrder()->getRevshareFeeDst() / 100;
        return ($base * (1 - $modifierBase));
    }
}