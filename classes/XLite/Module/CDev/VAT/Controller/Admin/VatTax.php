<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\Controller\Admin;

/**
 * Taxes controller
 */
class VatTax extends \XLite\Controller\Admin\AAdmin
{

    /**
     * Return the current page title (for the content area)
     *
     * @return string
     */
    public function getTitle()
    {
        return static::t('Taxes');
    }

    // {{{ Widget-specific getters

    /**
     * Get tax
     *
     * @return object
     */
    public function getTax()
    {
        return \XLite\Core\Database::getRepo('XLite\Module\CDev\VAT\Model\Tax')->getTax();
    }

    // }}}

    // {{{ Actions

    /**
     * Update tax rate
     *
     * @return void
     */
    protected function doActionUpdate()
    {
        $tax = $this->getTax();

        $name = trim(\XLite\Core\Request::getInstance()->name);
        if (0 < strlen($name)) {
            $tax->setName($name);

        } else {
            \XLite\Core\TopMessage::addError('The name of the tax has not been preserved, because that is not filled');
        }

        $optionNames = array(
            'display_prices_including_vat',
            'display_inc_vat_label',
            'ignore_memberships',
            'addressType',
            'merchant_tax_number',
            //'vatNumberChecker',
            'vatlayer_apikey',
        );

        foreach ($optionNames as $optionName) {

            $optionValue = isset(\XLite\Core\Request::getInstance()->$optionName)
                ? \XLite\Core\Request::getInstance()->$optionName
                : 'N';

            if ('vatlayer_apikey' == $optionName) {
                $optionValue = (string) \XLite\Core\Request::getInstance()->$optionName;
            } 

            if ('display_inc_vat_label' == $optionName) {
                $allowedOptionValues = array(
                    \XLite\Module\CDev\VAT\View\FormField\LabelModeSelector::DO_NOT_DISPLAY,
                    \XLite\Module\CDev\VAT\View\FormField\LabelModeSelector::PRODUCT_DETAILS,
                    \XLite\Module\CDev\VAT\View\FormField\LabelModeSelector::ALL_CATALOG,
                );
                $optionValue = in_array($optionValue, $allowedOptionValues) ? $optionValue : $allowedOptionValues[0];
            }

            $optionData = array(
                'name'     => $optionName,
                'category' => 'CDev\\VAT',
                'value'    => $optionValue,
            );
            \XLite\Core\Database::getRepo('XLite\Model\Config')->createOption($optionData);
        }

        // Set VAT base properties
        $vatMembership = \XLite\Core\Request::getInstance()->vatMembership;
        $vatMembership = $vatMembership
            ? \XLite\Core\Database::getRepo('XLite\Model\Membership')->find($vatMembership)
            : null;
        $tax->setVATMembership($vatMembership);

        $vatZone = \XLite\Core\Request::getInstance()->vatZone;
        $vatZone = $vatZone
            ? \XLite\Core\Database::getRepo('XLite\Model\Zone')->find($vatZone)
            : null;
        $tax->setVATZone($vatZone);

        $list = new \XLite\Module\CDev\VAT\View\ItemsList\Model\Rate;
        $list->processQuick();

        $rates = \XLite\Core\Database::getRepo('XLite\Module\CDev\VAT\Model\Tax\Rate')->findBy(array('tax' => null));
        foreach ($rates as $rate) {
            $tax->addRates($rate);
            $rate->setTax($tax);
        }

        \XLite\Core\TopMessage::addInfo('VAT / GST settings and rates have been updated successfully');
        \XLite\Core\Database::getEM()->flush();
    }

    /**
     * Switch tax state
     *
     * @return void
     */
    protected function doActionSwitch()
    {
        $tax = $this->getTax();
        $tax->setEnabled(!$tax->getEnabled());
        \XLite\Core\Database::getEM()->flush();
        $this->setPureAction(true);

        if ($tax->getEnabled()) {
            \XLite\Core\TopMessage::addInfo('VAT / GST has been enabled successfully');

        } else {
            \XLite\Core\TopMessage::addInfo('VAT / GST has been disabled successfully');
        }
    }

    /**
     * Expand common settings section
     *
     * @return void
     */
    protected function doActionExpand()
    {
        $this->toggleCommonSettingsDisplayMode(true);
    }

    /**
     * Collapse common settings section
     *
     * @return void
     */
    protected function doActionCollapse()
    {
        $this->toggleCommonSettingsDisplayMode(false);
    }

    /**
     * Update common settings section visibility mode
     *
     * @param boolean $value Visibility mode: true - section is expanded; false - collapsed
     *
     * @return void
     */
    protected function toggleCommonSettingsDisplayMode($value)
    {
        $optionData = array(
            'name'     => 'common_settings_expanded',
            'category' => 'CDev\\VAT',
            'value'    => $value,
        );

        \XLite\Core\Database::getRepo('XLite\Model\Config')->createOption($optionData);
    }

    /**
     * Define the actions with no secure token
     *
     * @return array
     */
    public static function defineFreeFormIdActions()
    {
        return array_merge(parent::defineFreeFormIdActions(), array('switch', 'expand', 'collapse'));
    }

    // }}}
}
