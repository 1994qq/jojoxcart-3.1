<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XLite\Module\CDev\VAT\View\FormField;

/**
 * Label 'Inc/exc.VAT' display mode selector widget
 */
class LabelModeSelector extends \XLite\View\FormField\Select\Regular
{
    /**
     * Test/Live mode values
     */
    const DO_NOT_DISPLAY  = 'N';
    const PRODUCT_DETAILS = 'P';
    const ALL_CATALOG     = 'Y';

    /**
     * getDefaultOptions
     *
     * @return array
     */
    protected function getDefaultOptions()
    {
        return array(
            static::DO_NOT_DISPLAY  => static::t('Never'),
            static::PRODUCT_DETAILS => static::t('On product details only'),
            static::ALL_CATALOG     => static::t('On all catalog pages'),
        );
    }
}
